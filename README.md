personal website and blog

[![Build Status](https://travis-ci.org/yufengwng/yufengwng.github.io.svg?branch=main)](
https://travis-ci.org/yufengwng/yufengwng.github.io)

\# get started

1. clone this repo
2. install [cobalt.rs](https://github.com/cobalt-org/cobalt.rs)
3. run `cobalt serve --drafts`

\# branches

- `master` stages the site
- `main` the source of truth, triggers deploy
