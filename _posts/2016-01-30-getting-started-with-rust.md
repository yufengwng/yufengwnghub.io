---
title: Getting Started With Rust
tags: [rust]
---

Rust is a very interesting programming language that promises memory safety,
easy concurrency, and speed. It’s a compiled language that plays in the systems
programming realm, with the likes of C and C++. Rust has a unique ownership
model and a sophisticated type inference system, among other things. Check out
their [documentation][doc] for all the technical goodness.

Let us explore some concepts in Rust by building something simple. First thing
that comes to mind is probably a Hello World program. Let’s not do that. You
probably know what that looks like already. Instead, we’re going to see how we
can do I/O in Rust, particularly getting and displaying things to an end user.
And instead of primitively compiling our code using the `rustc` compiler, we
are going to use `cargo`, the Rust package manager. Let’s run the `cargo`
command with the `--bin` flag to create a new project for an application named
`user_io`.

```bash
$ cargo new --bin user_io
$ cd user_io && tree .
.
├── Cargo.toml
└── src
    └── main.rs

1 directory, 2 files
```

Taking a look at what `cargo` generated, we see a `.toml` configuration file
and a source file. Taking a look at the main source file, we see a dead simple
Hello World program:

```rust
fn main() {
    println!("Hello, world!");
}
```

Let’s compile and run the program to make sure we are good to go:

```bash
$ cargo run
   Compiling user_io v0.1.0 (file:///home/yufeng/kata/rust/user_io)
     Running `target/debug/user_io`
Hello, world!
```

Nice! We are now ready to do some serious user I/O with Rust.

### Round 1: Prompt for Input

We need to prompt the user for some input. To make it simple, let’s ask the
user for their name and spit it back out for them. That will be a good warm-up
for us to flex our I/O muscles. As we saw earlier in the source code,
`println!` is a macro that will print a line of text to `stdout`. The bad news
for us is that it adds a newline character to the printed message, which will
not be user-friendly (think shell prompts).  The `println!` macro has a friend
called `print!` that does exactly what we want: print a message without a
newline and leave the cursor blinking at the end of the message.

Now that we understand how to print a prompt, it's time to think about getting
what the user typed. The Rust standard library provides I/O facilities that
allow us to read from standard input, by getting a handle on `stdin` and
calling the `read_line()` method on it. The tricky part is that the method
takes a mutable string buffer to store the retrieved data. What does that mean?
That means we have to create a variable binding to a `String` and ensure it is
`mut`-able, aka the value of the variable binding can be changed. You see, Rust
prefer things to be immutable so every variable you create cannot be changed
afterwards unless you explicitly state it is mutable.

Let’s put this all together in the source, and run it:

```rust
use std::io;

fn main() {
    print!("Enter name: ");

    let mut buffer = String::new();
    io::stdin().read_line(&mut buffer);
    println!("Hello, {}!", buffer);
}
```

Here is the output from `cargo run`:

```bash
$ cargo run
   Compiling user_io v0.1.0 (file:///home/yufeng/kata/rust/user_io)
src/main.rs:7:5: 7:40 warning: unused result which must be used, #[warn(unused_must_use)] on by default
src/main.rs:7     io::stdin().read_line(&mut buffer);
                  ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     Running `target/debug/user_io`
yufeng
Enter name: Hello, yufeng
!
```

Okay, it compiled and ran, but everything looks wrong. Don’t be scared, we can
figure this out together. A couple things here of note:

1. A warning from the compiler about unused result.
2. Prompt was not printed as desired, but program was still waiting for input.
   Prompt was finally printed when we print the name.
3. Our exclamation point is printed on a separate line.

Going through the documentation, we easily see the reason and solution to the
above points. For the first point, `read_line()` returns a `Result<usize>`
telling us how many bytes it read. Methods in Rust typically use the `Result`
wrapper for I/O operations, since those are prone to failure. This tells us
that there might be actual data returned, or it might be an error message.
Either way, we are not concerned about this result from `read_line()` right
now, so we can ignore it. A typical way to do so is to call `unwrap()` to pull
out the thing inside `Result`.

For the second point, the printing utilities actually queue up the messages in
buffers, so we need to call `flush()` to have our prompt printed out
immediately. In order to use `flush()`, however, we need to import the `Write`
trait into our scope.

For the third point, the `read_line()` method actually reads everything from
the user input, including the newline character, into the given buffer. Thus,
we need to `trim()` the buffer to get rid of the newline character before
printing it with `println!`. Let’s put all this together:

```rust
use std::io;
use std::io::Write;

fn main() {
    print!("Enter name: ");
    io::stdout().flush().unwrap();

    let mut buffer = String::new();
    io::stdin().read_line(&mut buffer).unwrap();
    println!("Hello, {}!", buffer.trim());
}
```

And here is our output:

```bash
$ cargo run
   Compiling user_io v0.1.0 (file:///home/yufeng/kata/rust/user_io)
     Running `target/debug/user_io`
Enter name: yufeng
Hello, yufeng!
```

Sweet! We can now print out a prompt and get user input.

### Round 2: Converting Input

We have covered quite a lot, but we need to level up and move on to
manipulating user input. Particularly, we will like the user to give us a
number, and we will store it as, well, a number. Rust has various data types
for numbers, each with an explicit size. We are going to be conservative and
use `i32`, meaning a 32-bit integer. We have learned to print a prompt and get
user input as a string, so we will leverage that here as well. Now, how do we
convert a string to a number? Rust `String`s actually has a nice method that
allows us to `parse()` the string into another type. And to help out Rust's
type inference, we will explicitly specify the type to be `i32`. Let's see what
it looks like:

```rust
...

print!("Enter number: ");
io::stdout().flush().unwrap();

io::stdin().read_line(&mut buffer).unwrap();
let num = buffer.trim().parse::<i32>().unwrap();
println!("Your number: {}", num);
```

Oh no. When we run this, it panicked saying an error with parsing:

```bash
thread '<main>' panicked at 'called `Result::unwrap()` on an `Err` value: ParseIntError { kind: InvalidDigit }', src/libcore/result.rs:731
An unknown error occurred
```

We see that we are reusing the buffer string here (since we are lazy and
conservative), so the number got added to the previous input, making `parse()`
unhappy. Let's make sure to `clear()` the buffer so we only get the number in
there. Here is the whole thing:

```rust
use std::io;
use std::io::Write;

fn main() {
    print!("Enter name: ");
    io::stdout().flush().unwrap();

    let mut buffer = String::new();
    io::stdin().read_line(&mut buffer).unwrap();
    println!("Hello, {}!", buffer.trim());

    print!("Enter number: ");
    io::stdout().flush().unwrap();

    buffer.clear();
    io::stdin().read_line(&mut buffer).unwrap();
    let num = buffer.trim().parse::<i32>().unwrap();
    println!("Your number: {}", num);
}
```

And in full action:

```bash
$ cargo run
   Compiling user_io v0.1.0 (file:///home/yufeng/kata/rust/user_io)
     Running `target/debug/user_io`
Enter name: yufeng
Hello, yufeng!
Enter number: 42
Your number: 42
```

### Bonus Round: Vectors and Debug Printing

Maybe one number is not enough. What if we want a couple numbers from the user?
Here's the challenge: Have the user type space-delimited numbers and store all
of the numbers in our program. We can easily accomplish this by manipulating
the input and storing them in a vector. `vec` is Rust's version of a growable
array, like Ruby's `Array` or Python's `List`.

We're going to use several advanced concepts here, so get ready to roll. After
we get the input as a string, we will split the string by spaces, iterate
through the collection of non-empty substrings, and parse each into an integer.
And for good measure, we will print out the vector. The print facility has a
nice way for us to "debug" print by using `{:?}`, meaning we can print out the
values in the vector in a nice array format. Here we go:

```rust
...

print!("Give me some numbers: ");
io::stdout().flush().unwrap();

buffer.clear();
io::stdin().read_line(&mut buffer).unwrap();
let vec: Vec<i32> = buffer.trim().split(' ')
    .filter(|s| !s.is_empty())
    .map(|s| s.parse::<i32>().unwrap())
    .collect();
println!("I got: {:?}", vec);
```

If you followed all of that, added them to the source and ran it, then we will
get a nice little array display:

```bash
$ cargo run
...
Give me some numbers: 1 2 3 4 5 -25 42
I got: [1, 2, 3, 4, 5, -25, 42]
```

All in all, our exploration here is brief and lacks major details and
explanations. So don't stop here. Check out the Rust [documentations][doc],
[examples][ex], and [keep playing][play] with Rust.

[doc]: https://doc.rust-lang.org/
[ex]: http://rustbyexample.com/
[play]: https://play.rust-lang.org/
