---
title: A Migration Journey to a Rust and Travis Powered Blogging Workflow
tags: [ci, gh-pages, rust, travis]
---

Recently, I have been thinking of resuming my blogging activities. I opened up
a terminal, `cd`-ed into my blog repo, and did a quick `ls`. I stared at the
file listing output. It has been a couple months since I have been in this repo
and written a blog post. What are all these files? Where do I begin? Some other
things raced through my mind:

> *"How do I run this thing again?"*
>
> *"Oh, I'm using Jekyll. Right. Let's fire that up...
> Wait, there's a gemfile so maybe I need to install some stuff."*
>
> *"What's the command for that again? Oh, something about bundler.
> No, I need Ruby first."*
>
> *"Where is my Ruby? What version am I using?
> Is this the system Ruby or something else?"*
>
> *"There's that rvm thing too. Do I have that, and how does it work again?"*
>
> ...

It was not actually that bad in reality. But the situation was a bit
aggravating since I was on a different machine where I don't normally use Ruby
on. Instead of trying to setup the Ruby toolchain, I started to look for other
alternatives. Perhaps there's something that does not require installing a
bunch of things. Maybe there's something that lets me simplify my blogging
workflow or trim down my files.

## Finding an Alternative

I'm a big fan of Rust, and I remember seeing mentions of static site generators
written in Rust some time ago. This seems like a great opportunity to try them
out. And alas, I found Keats' [gutenberg][gutenberg]. It looks very promising.
I just need to install a single executable and it comes with a bunch of
features. And I like that TOML is used for configuration and frontmatters; it
seems very fitting of a Rust project. Looking through the documentation, the
directory structure is very different from Jekyll so I will have to do some
work to port my blog posts over, but it's no biggie. Well, it wouldn't hurt
to try it out, so I made a branch and started digging in.

Installing gutenberg was easy enough; I was on a Mac, so I just needed to fire
up `brew` and I'm ready to go. Getting a basic configuration wasn't so bad
either; the only caveat was that I had an existing repo but the CLI doesn't yet
support generating config in the *current* directory. I just ran the CLI
somewhere else and copied over the scaffolds. My goal is to have a basic setup
going, and leave transitioning my blog posts until I actually commit to it. So
to see that things actually work, I tried doing the index page. This part was a
bit more involved, since I have to learn about how gutenberg does templates and
rendering. It took me a while to figure out, but I got `gutenberg serve` to
finally show something... something that looks funny.

I have a very simply index page at `content/_index.md`

```
+++
title = "test page"
+++

test
```

And this straightforward template at `templates/index.html`

{% raw %}
```html
<!DOCTYPE html>
<html>
  <head>
    ...
  </head>
  <body>
    <div>{{ section.content }}</div>
  </body>
</html>
```
{% endraw %}

But I was getting something funny in the DOM:

```html
...
<body>
  <div><p>test</p></div>
</body>
...
```

Typing out the code makes it look okay, but the stuff inside the `div` was
being rendered as _text_, not html. It turns out the `p` html tags were escaped
to html entities instead of being dropped in as html elements. Hmm, either
there's something up with my config or I need to dig into the Tera templating
language. Anyways, this seems like a good stopping point, so maybe I will check
out another alternative...

There was enough friction with gutenberg that I started looking at
[cobalt.rs][cobalt] as an alternative. If you're like me and started with
gutenberg and looked at the comparison chart on their github, then you might be
a bit biased _against_ cobalt since it looks like it was a bit lacking in terms
of features when compared to gutenberg. But reading through cobalt's
documentation made me think otherwise.

At first glance, the cobalt CLI looks very pleasant. The CLI can scaffold in
the current directory, which gutenberg didn't have. And it has commands to
create and publish new posts. I'm not exactly sure what those commands will do,
but they reminded me of what I tried to do currently with my `Rakefile` for
creating and publishing draft posts. Next up, the directory structure... looks
like... almost exactly the same as my Jekyll setup! Nice, this means there will
be less work to port things over! Let's see, the config and frontmatter... Oh,
it's yaml! I know how that works. Cobalt uses the liquid templating language
just like Jekyll, and the template variables look awfully similar... I was
getting excited. As I read more of cobalt's documentation, it makes me think
that cobalt is the Rust implementation of Jekyll. All of this boils down to
mean that there's a lot less stuff to learn and less work to migrate to cobalt.

I wanted gutenberg to work (I really like the name!), but it's clear that
cobalt does exactly what I needed. I think I'm all-in with cobalt.

[gutenberg]: https://github.com/Keats/gutenberg
[cobalt]: https://github.com/cobalt-org/cobalt.rs

## Migrating to cobalt.rs

Although cobalt does not have OS distribution packages or a `brew` formula yet,
installing from source doesn't seem that bad when I have Cargo.  Plus, I'm sure
there's something for it in the Arch Linux AUR. Just as I suspected, there's a
package for cobalt:

```bash
$ yaourt cobalt
1 aur/cobalt 0.12.1-1 (3) (0.03)
    Static site generator written in Rust
...
```

One hiccup I ran into while trying to install cobalt is getting it to build.
Understandably, the PKGBUILD script for cobalt has a `makedepends` on `cargo`
since we need that to build cobalt from source. However, I installed rust
through the rustup script and so the build process does not recognize the fact
that I already have cargo. Let's work around this by editing the PKGBUILD and
removing `cargo` from the `makedepends` array.

But that's not the end of it! Apparently, one of the crates in cobalt's
dependency chain, `onig_sys`, needs `cmake` for its build. I don't have `cmake`
installed, for reasons. The cobalt documentation does say to install `cmake`
first, so I suppose this is expected. Since I don't really want `cmake` laying
around but I need it for the build, I ended up doing this for the PKGBUILD
script:

```bash
...
makedepends=('cmake')
...
```

...and it builds! Now I have:

```bash
$ cobalt --version
Cobalt 0.12.1
```

Next up is configuration and porting over my files to cobalt. This part is
pretty straightforward since there's a close mapping between Jekyll and cobalt
conventions. Running `cobalt init` on my repo gave me a starter `_cobalt.yml`
config file, and then I just needed to play around and tweak it. For my blog
post frontmatters, I just needed to rename `tags` to `categories`, since that's
the only thing cobalt has. And then some juggling with `_layouts` and
`_includes` templates.

One thing I do miss from Jekyll is being able to add arbitrary fields in the
frontmatter. I have a custom `banner` field that I use to specify a cover image
for a blog post. In Jekyll, I can just drop it into the frontmatter and have it
show up as an object `page.banner` in my liquid templates. For cobalt, any
custom fields has to be put under the `data` field, which is fine, no biggie.

An issue I ran into has something to do with cobalt's support for liquid. For
example, let's go back to the `banner` field I mentioned. Some blog posts will
have this field, while some will not. If I have this snippet in my templates:

{% raw %}
```liquid
{% if page.data.banner %}
  ... do stuff with banner image ...
{% endif %}
```
{% endraw %}

This will work fine in Jekyll, but cobalt complains that it did not find
`banner` under the `page.data` object. Well... it used to. This seems to be
fixed in version 0.12 (I was using 0.11 when I encountered this). The lesson
here is that some features might not be supported in cobalt yet. And that's
fine with me; I'm not doing anything complicated with liquid templates. The
migration to cobalt was a bit bumpy (with most faults mine) but overall a
pleasant experience.

## Setting up Travis for Deployment

Now comes the more interesting part. You see, GitHub only supports using Jekyll
for GitHub Pages. If you use Jekyll for your project, GitHub will build it and
push the generated artifacts to your GitHub Pages website. On the flip side,
this means that when I commit new blog posts and push to GitHub, nothing will
happen. My actual blog website will not be updated since I'm using cobalt.

But fear not! Travis and CI automation comes to the rescue! Typically, Travis
CI is used with GitHub projects to build and test code. However, we're going to
set it up to build our site and "deploy" that to GitHub Pages. There's some
nice documentation from both [cobalt][cobalt-deploy] and
[gutenberg][gutenberg-deploy]. Travis even have their own
[documentation][travis-deploy] on that.

I will admit that I have not used Travis before; I just didn't have a need for
it yet. (I do use Jenkins, so I at least know what this stuff is about).  The
Travis [landing page][travis-landing] is making a solid sell of the service, so
I'm pretty excited to get started. I wouldn't really call my project an open
source project, but more like an open prose project. In any case, I will gladly
take up on the 10000% free offer. So, let's dig in!

First things first, let's sign up with Travis. I clicked the "Sign Up" button,
and boom! I'm in the dashboard. That was easy. I may have lied that I have not
used Travis before. I sort of recall fiddling with the Travis dashboard and
looking at logs some time ago... Anyways, time to look around and get familiar
with how things work. Okay! I see my project in the Profile page, so all I got
to do is flick the switch... and its enabled!

Let's commit a very simple `.travis.yml` config file to test that it works:

```bash
$ echo 'script: "echo hello world"' > .travis.yml
$ git add .travis.yml && git commit && git push
```

...and the build is triggered! Here's the log output:

```
... 400 lines of system info and versions ...

$ echo hello world
hello world


The command "echo hello world" exited with 0.

Done. Your build exited with 0.
```

Sweet, things look good. Now I can start tinkering with the actual
configurations to get travis to build and deploy my site. Before I go ahead and
write the config, let's figure out what do I actually need Travis to do for me.
And it's basically two things:

1. Somehow get cobalt.rs and build my site with it.
2. Deploy the generated site to GitHub Pages.

Sounds easy enough. Let's tackle the first requirement. I need to get cobalt.rs
into Travis before I can use it. Understanding the build environment on Travis
will be a good first step. From Travis' documentation on [their
environments][travis-env], it looks like I get a Docker container on Amazon EC2
running Ubuntu Trusty by default. That sounds good to me; the boot time is fast
and there's plenty of resources. Although I don't think I need it, I'm going to
put it in the config anyways to remind myself of the environment and
future-proof this a bit. Let's start fresh with `.travis.yml` and put this at
the top:

```yaml
sudo: false
dist: trusty
```

Next, it looks like the Trusty environment already comes with a lot of [nice
tools][travis-env-tools] installed. Cobalt.rs is a pretty new tool, so it's
obvious that it won't be pre-installed. And the major issue here is that cobalt
does not have official Linux distribution packages yet, as the
[documentation][cobalt-install] mentioned. I can, however, download a binary or
build from source. But the best option seems to be using [trust][gh-trust] to
install a binary, which cobalt seems to recommend. The `japaric/trust` project
helps Rust projects build and test their stuff across environments, and there's
a nice [script][gh-trust-script] that others can use to install binaries. Those
binaries must be built using the `japaric/trust` setup, but since cobalt
recommends this way I'm going to assume that is the case for cobalt.rs
binaries.

So I now have an idea of what command to use to install cobalt in the Travis
build environment, but where do I put it? Let us now understand how Travis does
things and how the build process works. The [build
lifecycle][travis-build-cycle] seems pretty straightforward. Travis will
install dependencies, run the build, and do an optional deployment, with other
optional steps inbetween. Each of those steps can be customized with my own
commands. From this, I picked out three steps that's most relevant to what I
need:

- `install` this is where I specify installing cobalt
- `script` this is where I run cobalt to build the site
- `deploy` this is where I specify the deployment to GitHub Pages

With that out of the way, let's see how we can use the `japaric/trust` install
script:

```bash
$ curl -LSfs https://japaric.github.io/trust/install.sh | sh -s -- --help
Install a binary release of a Rust crate hosted on GitHub

Usage:
    install.sh [options]

Options:
    -h, --help      Display this message
    --git SLUG      Get the crate from "https://github/$SLUG"
    -f, --force     Force overwriting an existing binary
    --crate NAME    Name of the crate to install (default <repository name>)
    --tag TAG       Tag (version) of the crate to install (default <latest release>)
    --target TARGET Install the release compiled for $TARGET (default <`rustc` host>)
    --to LOCATION   Where to install the binary (default ~/.cargo/bin)
```

That `curl` command just says to download the `install.sh` script. I then pipe
it to the `sh` shell and pass the `--help` flag to the install script itself to
get it's help message. And from that output, I see that we have a couple flags
that we can use. The `--git` and `--crate` flags seem pretty straightforward;
they will just mention cobalt. I will probably want to use `--force` for good
measure. I know that the Travis environment is Ubuntu, so the `--target` flag
should mention something about Linux. And lastly, I can use the `--tag` flag to
pin cobalt to a specific version. Now, let's put all that into the
`.travis.yml`, right after the environment settings:

```yaml
install:
  - curl -LSfs https://japaric.github.io/trust/install.sh |
    sh -s --
    --force
    --git cobalt-org/cobalt.rs
    --crate cobalt
    --target x86_64-unknown-linux-gnu
    --tag v0.12.1
  - export PATH="$PATH:~/.cargo/bin"
```

The cobalt binary gets installed to `~/.cargo/bin` by default, so I need to
make sure it's in the `PATH` before I start using it. Next, I need to run
cobalt to build my site, which should just be a one-liner. Let's add this to
the `.travis.yml` after the `install` step:

```yaml
script:
  - cobalt build
```

At this point, I have tackled the first requirement I listed above. Let's
commit what I have so far to make sure everything works:

```bash
$ git add .travis.yml && git commit && git push
...

# travis build log
...
[info] Build successful
The command "cobalt build" exited with 0.
...
```

The second requirement I need to tackle is deploying the generated site to
GitHub Pages. What's nice about Travis is that there is already build-in
support for doing a deploy to GitHub Pages; all I need to do is put in the
right config in `.travis.yml`. One little thing I need to do beforehand is to
[setup a token][travis-gh-token] for Travis to use. A watered-down version of
how GitHub Pages work is that GitHub will take files on one of my repo branches
and publish them as my website. So what I'm trying to ask Travis to do is to
take the generated files from cobalt and push them to my repo on GitHub. Travis
will need an access token to do that. In my GitHub Settings, I went into
`Developer settings > Personal access tokens` and generated a new token with
only the `public_repo` scope. I then added it as a new environment variable
named `GITHUB_TOKEN` under the settings page of my Travis project.

Now that credentials are taken care of, let's figure out what config I need for
the `pages` [deploy provider][travis-deploy]. I will definitely need
`skip_cleanup: true` to prevent Travis from cleaning up the generated files
from cobalt, which is exactly the stuff I need it to push. Specifically, I'm
setting `local_dir: _site` so that Travis pushes all the files in the `_site`
directory, which is where cobalt will generate my site. I'm also going to use
`keep_history: false`. This asks Travis to overwrite the history on the branch
it pushes to (the target branch). Since this target branch is only used for
deploying my site, it will only accumulate these deploy commits if we don't
overwrite the git history. So let's have Travis overwrite it to keep it a bit
cleaner.

The part that might seem odd is that I'm setting the `target_branch` to
`master`. Typically, you will use the `gh-pages` branch for the GitHub Pages
feature. However, for personal repos (like mine) GitHub will use the `master`
branch instead. What this means in practice is that I will have the `master`
branch as just a "deploy" branch that I'm not going to touch. Instead I will
create a new branch (called `main`) that I will do stuff on. This will be the
branch where all my code and blog posts will live. This is also where we have
the `.travis.yml` file to control the Travis process. In fact, let's do that
right now before I get going with the whole deploy thing:

```bash
$ git checkout -b main
$ git push -u origin main
```

And then in my repo `Settings > Branches`, I changed the "Default branch" to
`main` so now everything about my repo is based on the `main` branch. One last
thing I want to ensure is to restrict the Travis deploy process to only when I
update the `main` branch. In other words, only the `main` branch should trigger
a deployment of my site, and the `master` branch is used to reflect the actual
files for my site. This is done by setting the `deploy.on.branch` field to
`main`. So now I have all the deploy config I need; let's add it to the bottom
of `.travis.yml`:

```yaml
deploy:
  provider: pages
  github_token: $GITHUB_TOKEN
  skip_cleanup: true
  keep_history: false
  local_dir: _site
  target_branch: master
  on:
    branch: main
```

Great! That pretty much covers setting up Travis to deploy my site. I now have
a workflow where I can edit blog posts locally using cobalt, then push to the
`main` branch and have Travis deploy my site to GitHub Pages. But we're not
done yet! When things start to work, it's only the beginning.

One big issue I noticed is that we're lacking sass support in cobalt. It does
support compiling sass to css, but you need to have a version of the binary
that was built with that feature flag. The cobalt binary I'm using in my Travis
build process was not built with the `sass` feature, so my published site looks
a little funny without those css styles... We can either update the `install`
step in `.travis.yml` to compile cobalt from source, or we can wait for a new
release of cobalt that does have sass enabled by default (which is the plan
from what I read on GitHub). Also, I noticed that Travis defaulted my build to
a ruby language environment, meaning it went ahead and installed a bunch of
ruby tools. I don't use those tools in the build at all, so it's just wasted
work and slows down the build. There ought to be a way to disable that...

There are also other things about the workflow that I can improve on. But I
have covered a lot of ground in this post already, so I'll call it a wrap!

[gutenberg-deploy]: https://www.getgutenberg.io/documentation/deployment/github-pages/
[cobalt-deploy]: http://cobalt-org.github.io/docs/deployment.html
[cobalt-install]: http://cobalt-org.github.io/docs/install.html
[travis-landing]: https://travis-ci.org/
[travis-deploy]: https://docs.travis-ci.com/user/deployment/pages/
[travis-env]: https://docs.travis-ci.com/user/reference/overview/#Virtualisation-Environment-vs-Operating-System
[travis-env-tools]: https://docs.travis-ci.com/user/reference/trusty/#Environment-common-to-all-Trusty-images
[travis-build-cycle]: https://docs.travis-ci.com/user/customizing-the-build/#The-Build-Lifecycle
[travis-gh-token]: https://docs.travis-ci.com/user/deployment/pages/#Setting-the-GitHub-token
[gh-trust]: https://github.com/japaric/trust
[gh-trust-script]: https://github.com/japaric/trust#use-the-binary-releases-on-travis-ci
