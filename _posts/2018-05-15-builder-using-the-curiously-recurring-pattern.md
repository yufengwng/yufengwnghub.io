---
title: Inheriting from a Builder Using the Curiously Recurring Pattern
tags: [design-patterns, java]
---

The builder pattern is really handy when you're working with complex objects.
If the complex objects come in an inheritance hierarchy, you may still apply
the builder pattern. You can specify a builder for the `final` subclasses. You
can even define a builder for any one of the intermediary concrete classes.
Since your classes will likely share common fields via inheritance, you might
notice some duplication of code in your builders in the form of field setters.
How do we go about sharing code in our builders by leveraging inheritance just
like the complex objects that the builders help construct?

To frame our exploration of the problem, let us look at building cars.

```java
abstract class Car {
    protected String color;
    protected String engine;
    ...
}

final class Sedan extends Car {
    private boolean moonroof;
    ...
    public static final Builder {
        private String color;
        private String engine;
        private boolean moonroof;

        public Builder color(String color) {
            this.color = color;
            return this;
        }

        public Builder engine(String engine) {
            this.engine = engine;
            return this;
        }

        public Builder moonroof(boolean moonroof) {
            this.moonroof = moonroof;
            return this;
        }

        public Sedan build() {
            return new Sedan(...);
        }
    }
}

final class Truck extends Car {
    private boolean trailer;
    ...
    public static final Builder {
        private String color;
        private String engine;
        private boolean trailer;

        public Builder color(String color) {
            this.color = color;
            return this;
        }

        public Builder engine(String engine) {
            this.engine = engine;
            return this;
        }

        public Builder trailer(boolean trailer) {
            this.trailer = trailer;
            return this;
        }

        public Truck build() {
            return new Truck(...);
        }
    }
}

// ...and then use it like this
Sedan s = new Sedan.Builder()
                   .color("atlantic blue")
                   .engine("2.4 liters")
                   .moonroof(true)
                   .build();
```

It works, but... Ughh, look at the repetitiveness of those setter methods in
`Sedan.Builder` and `Truck.Builder`. We only have 3 fields in this example, but
imagine if we have 10+ fields to take care of... Let's make this better by
introducing an abstract builder in the shared base class.

```java
abstract class Car {
    ...
    protected static abstract class CarBuilder {
        protected String color;
        protected String engine;

        public CarBuilder color(String color) {
            this.color = color;
            return this;
        }

        public CarBuilder engine(String engine) {
            this.engine = engine;
            return this;
        }
    }
```

And then our car subclass builders can be simplified to handle just their
respective differences. For `Sedan.Builder` it will be:

```java
public static final class Builder extends CarBuilder {
    private boolean moonroof;

    public Builder moonroof(boolean moonroof) {
        this.moonroof = moonroof;
        return this;
    }

    public Sedan build() {
        return new Sedan(...);
    }
}
```

Now this looks good on paper, but it doesn't actually work well in practice.
The inherited setters return an instance of `CarBuilder`, while the other
setters return an instance of `Sedan.Builder`. This means you cannot chain all
these method calls, and instead need to write individual statements.

## Making the Builders Curiously Recurring

The way we're going to enable nicer method chaining for the builders is to make
the abstract base builder generic. We will make `CarBuilder` parameterized on
`T`, where `T` is meant to be the subclass builder. The inherited setters will
then return a type `T` to allow us to do method chaining with the subclass
builder. But we need a way to return an instance of the subclass builder in the
inherited setters, so we add an abstract method for that. The implementation
will look like this:

```java
protected static abstract class CarBuilder<T> {
    ...
    protected abstract T self();

    public T color(String color) {
        this.color = color;
        return self();
    }
    ...
}

public static final class Builder extends CarBuilder<Builder> {
    ...
    @Override
    protected Builder self() {
        return this;
    }
    ...
}
```

Now the subclass builders inherit all the good stuff from `CarBuilder` and have
method chaining working the way we want it to. This is known as the [curiously
recurring template pattern (CRTP)][wiki-crtp] in C++, where the subclass shows
up as the base class's type argument, giving a curious sense of recursion. In
Java, you replace "template" with "generic" and it's pretty much done the same
way.

Should we call it a day now?  Not quite. Simply introducing generics doesn't
cut it; we can't really guarantee _all_ subclasses of our `CarBuilder<T>` will
do the right thing. The fact that the type parameter `T` is not bounded means
`T` can be anything. If a rebellious `Coupe` subclass comes along, it might do
something like this:

```java
final class Coupe extends Car {
    ...
    public static final class Builder extends CarBuilder<PhotoBuilder> {
        ...
        @Override
        protected PhotoBuilder self() {
            return new PhotoBuilder("awesome-coupe-photo");
        }
        ...
    }
}
```

Hmm, we were expecting a builder of cars and not a builder of car photos! We
need to constraint type `T` somehow, and the way we will do it is by adding a
type bound to `T` in the base class signature. In code, we will write
`CarBuilder<T extends CarBuilder<T>>`. In English, it says that `CarBuilder` is
generic over any class `T` where `T` is our subclass parameterized on `T`
itself. In plain English, it means the subclasses can't do any funny business.
(Or can it?)

This type bound solution is inspired by a similar [pattern in C#][csharp-crtp].
As that article points out, this type bound doesn't _actually_ make the
guarantees that we want. You might be a `EvilSedan` that extends a
`CarBuilder<Truck>` and cause some funny business. But hey, at least we got
close.

## Why the Builder Pattern?

So, we have seen the builder pattern and we have tried hard to make it
fool-proof. But why do we use the builder pattern anyways?  Let us now justify
why the builder pattern is awesome.

Probably the main reason why it rocks, and why everyone uses this pattern, is
that it allows you precise control over an object's construction process. You
can define setters for certain fields, you can define how inputs are verified,
and you can even _restrict_ [what and how][checked-builder] it gets built.

Second, a builder lets us bundle up data and pass it around during the
construction process. If your constructor has 10+ parameters, it kind of forces
us to have all of the arguments up-front. Using a builder, we can incrementally
provide those arguments. Maybe I will provide 3 of them to the builder here,
set 2 more over there, then pass the builder to another method to set the rest.
A builder lets my application construct an object at its own leisure, and then
call `build()` when the time is right to get the desired object.

Lastly, having a builder allows us to define our objects to be immutable! All
the mutations can be shoved into the builder, leaving our object void of
setters.

## Pairing with Static Factories

Another design pattern that I really like is the static factory method pattern.
The static factories give me a lot more control in how to construct an object,
and unlike constructors they're named (!) methods that I can reference. Instead
of calling `new Sedan.Builder()`, I will have a static method `Sedan.builder()`
instead. And if there are "required" arguments for the builder, then I can add
a static method for that:

```java
final class Sedan extends Car {
    ...
    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(String engine) {
        return new Builder().engine(engine);
    }
    ...
}
```

In conclusion, the builder pattern along with the curiously recurring pattern
makes constructing objects a lot nicer, while the static factory method pattern
makes it easy to get a builder itself.

[checked-builder]: https://dev.to/schreiber_chris/creating-complex-objects-using-checked-builder-pattern
[wiki-crtp]: https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern
[csharp-crtp]: https://blogs.msdn.microsoft.com/ericlippert/2011/02/03/curiouser-and-curiouser/
