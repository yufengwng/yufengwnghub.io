---
title: To The Next 1,000 Days
tags: [1000-days, new-years]
---

Tonight is New Year's Eve, a time when the year closes the curtains and a
fresh year takes on the stage. This change of season brings new possibilities
and new uncertainties. Facing uncertainties is a part of life, and having a
game plan will help immensely in the changing times.

Many people have New Year resolutions, or goals that they wish to get done by
the end of the new year. However, a year seems a bit short for some goals that
I might want to achieve. There is also the technique of creating 5 year plans,
which seemed to me like a ridiculously long time. Given how rapidly technology
is advancing, the entire world could change over in just 5 years. The idea of
1,000 days appeared to be just the right balance for me.

The team at [Maptia](https://maptia.com) wrote a beautiful piece of story
on [Medium][1] earlier this month. In the article, they shared ten valuable
lessons they learned from building their own startup company. Their journey
of 1,000 days seemed quite a lot to me, especially since that is roughly about
3 years. You can get a whole list of things done in that duration of time.

In three years, I will be graduating from college and the end of 1,000 days
falls right in that time frame. It will be the perfect occasion to reflect on
my 1,000 days as well as my college career shortly after I graduate. Of
course, having yearly write-ups will be important as well, but the write-up
at the end of my 1,000 days will be the most meaningful and contain my
overall reflections.

Whether it is New Year resolutions, 5 year plans, or 1,000 days, I hope
everyone set goals for themselves and strive to make yourself better than
before you started.

Before I end the post for the Year of 2014, and welcome the Year of 2015,
I will like to leave us with an inspirational quote that pretty much sums
up what you have to do for anything you want to accomplish:

> Start by doing what's necessary, then what's possible,
> and suddenly you are doing the impossible.
>
> *- St. Francis of Assisi*

[1]: https://medium.com/founder-stories/ten-things-we-believe-2c85c462d508
