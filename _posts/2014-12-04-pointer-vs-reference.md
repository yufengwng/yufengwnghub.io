---
title: Pointer vs Reference
tags: [cpp]
---

One of the first interview questions that I got asked in college was to
describe the difference between a pointer and a reference in C++. I hesitated
for a moment. I have used pointers, and I know what a reference is, but I
couldn't really put their differences into words. You see pointers used a lot
in C, and you find references in C++. Whenever someone saids they know C++, I
tend to ask them this particular question. This [stackoverflow question][1]
pretty much sums it all up, but here's my take on the subject. I won't go into
too much details of what a pointer or a reference really is, but will try to
highlight the differences and similarities between the two.

## Dependency

The first aspect I see that differs between a pointer and a reference is their
relationship to other objects. For a pointer, you can have it point to anything
else. You can even have it point to nothing, by assigning it to `NULL` or
`nullptr`. It has no dependency to other objects, since you can just declare a
pointer and not initialize it (which is not of best practice). On the other
hand, a reference depends on another existing object. When you declare a
reference variable, you have to initialize it to be a reference of some other
object. Along the lines of dependency, you can always re-assign the pointer to
point to something else, but a reference stays binded to the designated object.

## Syntactic Sugar

The syntax for creating a pointer and a reference variable is different, as
seen below.

```cpp
int var = 5;

int *ptr = nullptr;  // point to null
int *ptr2 = &var;    // point to another variable
int &ref = var;      // a reference
```

Under the hood, references are just syntactic sugar for `const` pointers,
meaning you cannot change the address it holds. If you see it this way, you can
equate `ref.var` to `(*ptr).var` as doing the same thing. We can see more
clearly the syntactic difference if we use them as function parameters. Using a
pointer parameter, you will have to dereference the pointer everytime you want
to change the variable it points to. The shorthand for dereferencing a pointer
and accessing its elements is the arrow operator `->`. If we use a reference
parameter, the syntax becomes much easier. We just need to use the dot
operator `.`.

```cpp
void ChangeMemberVariable(int *ptr, int &ref) {
    ptr->var = 5;
    ref.var = 5;
}
```

## Memory Usage

From my reading, most compilers implement references using pointers. This means
pointers and references take up the same amount of space in memory. A pointer
is just basically a variable that holds a memory address in hexadecimal form.
With that in mind, we can think of a reference as another variable like a
pointer, which holds the address of the object it binds to.

## Method Chaining

If the methods you are working with returns an object, you could easily
utilize method chaining to call several methods in a sequence. You could
accomplish method chaining if you return using a pointer or reference. Note
that you can't just return an object that is local to the method, since it
will be destroyed off the stack. Returning by reference to an object is
recommended since it simplifies the syntax and makes it easier to understand
that subsequent method calls are associated with the returned object.

Here is an example of a class that has methods that allow use of method
chaining.

```cpp
class MyChain {
  public:
    ...

    MyChain& printHello() {
        cout << "Hello" << endl;
        return *this;
    }

    MyChain& printWorld() {
        cout << "World" << endl;
        return *this;
    }
};
```

Notice that we are returning the object itself by dereferencing the `this`
pointer. We can then call the methods on an `MyChain` object by chaining the
calls.

```cpp
MyChain obj;
obj.printHello().printWorld();
```

## Verdict

Simply put, pointers give you control and references give you simplicity.
Pointer allows you to refer to any other object, but a reference stays binded
to one object. When using pointers you have to dereference, but using a
reference you can directly access the referred object itself. References are
very similar to pointers, with only a few restrictions away. If you're
interested, check out the [c++ fqa][2] for a good read on references.

[1]: http://stackoverflow.com/questions/57483/what-are-the-differences-between-a-pointer-variable-and-a-reference-variable-in
[2]: http://yosefk.com/c++fqa/ref.html
