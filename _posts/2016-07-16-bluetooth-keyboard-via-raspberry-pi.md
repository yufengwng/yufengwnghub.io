---
title: Bluetooth Keyboard via Raspberry Pi
tags: [bluetooth, python, raspberry-pi]
data:
  banner: /assets/img/blog/minimal_keyboard.jpg
---

If you have a spare USB keyboard lying around, you can hook it up with a
Raspberry Pi to emulate as a Bluetooth keyboard. It doesn't take much effort to
get a neat little prototype going. The idea is to connect your USB keyboard to
the Raspberry Pi, program the Pi to act as a Bluetooth keyboard, and connect to
the Pi on your Bluetooth-enabled device. This means you can control your laptop
or phone wirelessly within the Bluetooth range via this Raspberry Pi keyboard
setup.

There are quite a number of resources for doing various things with your Pi,
but a [tutorial by Liam Fraser][tutorial] is exactly what we're looking for to
setup the Pi to emulate a Bluetooth keyboard. In the tutorial, Liam guides you
through the process, providing code snippets and other files you will need to
get it working. Unfortunately, some of the instructions are outdated if you're
running newer versions of the Raspberry Pi operating system, like Raspbian
Jesse. Therefore, I will attempt to present updated information here on this
post. This post will reference Liam's tutorial quite a bit, but organized
differently based on my own experience with this. This post will assume that
you're familiar with Linux and Python.

## Bluetooth Adapter

The first thing we need to do is make sure we have a Bluetooth adapter on our
Raspberry Pi. If you have a Raspberry Pi 3, you're all set since its already
built-in. If you have the first generation of the Pi like me, make sure to
invest in a Bluetooth adapter. And perhaps add in a USB-to-serial cable as
well, so you can connect to the Pi via serial and reserve the two USB ports for
the Bluetooth adapter and USB keyboard. Let's run the following command at the
terminal to check out our Bluetooth adapter:

```bash
$ hciconfig
```

You should be able to see the name of your Bluetooth adapter, for example
`hci0`. We will call this the "interface name". Make sure the adapter is
powered on and says `UP`. If it's `DOWN`, run the following to turn it on:

```bash
$ sudo hciconfig <interface name> up
```

## Required Packages

Let's make sure we have the necessary software packages installed on our Pi.
The Raspbian operating system is based on Linux, and `bluez` is the primary
software framework that manages the Bluetooth stack. The latest version, BlueZ
5, introduced API changes that are not compatible with the instructions in
Liam's tutorial, but we'll see how we can fix that.

We begin by installing the `bluez` framework. Make sure you run `apt-get
update`, and then install the following packages:

```bash
$ sudo apt-get install bluez bluez-firmware
```

We will be using Python for our development, and in particular Python 2.7 since
it has better support on the Pi. Let's install the necessary python packages:

```bash
$ sudo apt-get install python-dev python-pip python-gobject python-bluez
```

The first three packages should already come installed on your Raspbian
installation, but if not then install them. The last one is for the `pybluez`
[module][pybluez] that provides python APIs for Bluetooth.

Lastly, we will use `pip` to install a python package we need for working with
keyboard inputs:

```bash
$ sudo pip install evdev
```

## Bluetooth Service

On Raspbian Jesse, system services are now managed by `systemd` instead of init
scripts like older versions. Let's make sure the Bluetooth service is up and
running with the following command:

```bash
$ systemctl status bluetooth.service
```

If it is not active and running, then try to start it with the `systemctl
restart` command.

In Liam's tutorial, it was recommended that we disable the default Bluetooth
plugins. From my experience, I was only able to connect to my Bluetooth
keyboard service if I disable those plugins, so let's go ahead and disable
them. If we look at the config file `/etc/bluetooth/main.conf` as suggested in
the tutorial, there isn't a `DisablePlugin` option. What I found to work is by
specifying this in the Bluetooth service config file used by `systemd` by
passing them as flag options to `bluetoothd`, the Bluetooth service daemon.
Within the `/etc/systemd/system` directory, open the file
`bluetooth.target.wants/bluetooth.service`. And then edit the line with
`ExecStart` by adding the `noplugin` option. That line should now look like
this:

```
ExecStart=/usr/lib/bluetooth/bluetoothd --noplugin=network,input,audio,pnat,sap,serial
```

Save the edit, and then restart the Bluetooth service:

```bash
$ sudo systemctl daemon-reload
$ sudo systemctl restart bluetooth.service
```

Again, check that the service is up and running with the `systemctl status`
command. You should now see the `noplugin` option in the output. Also, check
`hciconfig` again to make sure the Bluetooth device is `UP`, since restarting
the service seems to power off the adapter.

## Keyboard Emulator

Once we have everything in place, we can start writing the Bluetooth keyboard
emulator in python. For the most part, you will have a working prototype by
following Liam's tutorial. Therefore, I will be brief in this post about the
actual code, but will highlight the necessary changes needed.

Before we begin, you will need some resources provided by Liam, so head over to
[the tutorial][tutorial] and download the zip file mentioned in "Step 06". In
particular, you will need the `keymap.py` and `sdp_record.xml` files. Then,
start a new project directory with those two files, and start a new file called
`pitooth.py` with executable permissions, which is where we will write our
code.

First, we create a class to manage the Bluetooth service and connections. The
following is the outline for the class:

```python
#!/usr/bin/python2.7

import bluetooth
import dbus
import evdev
import keymap
import os
import sys
import time
import uuid

class BluetoothManager(object):

    CTRL_PORT = 17
    INTR_PORT = 19

    def __init__(self):
        ...

    def listen(self):
        ...

    def send_input(self, report):
        ...
```

The list of `import`s cover everything we will need. The first `import` is for
the PyBluez module, and the fourth `import` is for utilities provided in the
`keymap.py` file. To emulate a Bluetooth keyboard, we expose a keyboard
service, which will use a control and interrupt Bluetooth connection. They
listen on port 17 and 19, respectively. In the constructor, we will setup our
Bluetooth adapter and connections:

```python
    def __init__(self):
        # Configure bluetooth device to be keyboard
        os.system("hciconfig hci0 class 0x002540")
        os.system("hciconfig hci0 piscan")

        # Define server sockets for communication
        self.scontrol = bluetooth.BluetoothSocket(bluetooth.L2CAP)
        self.sinterrupt = bluetooth.BluetoothSocket(bluetooth.L2CAP)

        # Bind sockets to a port
        self.scontrol.bind(('', BluetoothManager.CTRL_PORT))
        self.sinterrupt.bind(('', BluetoothManager.INTR_PORT))
        print("Control bound to port {}".format(BluetoothManager.CTRL_PORT))
        print("Interrupt bound to port {}".format(BluetoothManager.INTR_PORT))

        # Setup for service advertising via dbus
        self.bus = dbus.SystemBus()
        self.uuid = str(uuid.uuid1())
        self.manager = dbus.Interface(self.bus.get_object('org.bluez', '/org/bluez'),
                                      'org.bluez.ProfileManager1')

        # Read in the service record
        with open("./sdp_record.xml", "r") as f:
            self.service_record = f.read()
        print("Done reading sdp_record.xml")
```

The important part is the fourth block, where we setup `dbus`. We use `dbus` to
talk to the `bluez` APIs in order to advertise our Bluetooth keyboard service.
With the updates in BlueZ 5, we need to use the `ProfileManager` interface. The
[BlueZ 5 porting guide][guide] provides more details on this.

```python
    def listen(self):
        # Advertise service record
        opts = {'ServiceRecord': self.service_record}
        self.manager.RegisterProfile('/', self.uuid, opts)
        print("Service record added")

        # Listen on server sockets with 1 connection limit each
        self.scontrol.listen(1)
        self.sinterrupt.listen(1)
        print("Waiting for connection...")

        self.ccontrol, self.cinfo = self.scontrol.accept()
        print("Got a connection on the control channel from " + self.cinfo[0])

        self.cinterrupt, self.cinfo = self.sinterrupt.accept()
        print("Got a connection on the interrupt channel from " + self.cinfo[0])
```

In the `listen()` method, we advertise our keyboard service using the
`RegisterProfile` API. The service description is in the `sdp_record.xml` file,
which we have read into our program already. The first argument to that API
call is supposedly a dbus object path to your profile implementation, according
to [the documentation][doc] found in the `bluez` git source code. You can see
what the profile implementation involves by looking at the [test file][test].
In my experience, using `'/'` worked fine since our keyboard emulator is pretty
simple right now.

```python
    def send_input(self, report):
        # Convert keyboard input report to hex string
        hex_str = ''
        for element in report:
            if type(element) == list:
                # Convert bit array to a byte value
                bin_str = ''.join(map(str, element))
                hex_str += chr(int(bin_str, base=2))
            else:
                hex_str += chr(element)
        self.cinterrupt.send(hex_str)
```

The `send_input()` method provides a way for us to send keyboard events, called
"input reports", to the Bluetooth manager to send over the Bluetooth
connection. It will make more sense once we look at the actual keyboard
emulator, but in essence the method converts the byte values in the report into
a string for sending.

```python
class KeyboardEmulator(object):

    def __init__(self):
        ...

    def change_state(self, event):
        ...

    def event_loop(self, bt_man):
        ...
```

The above is the outline for the keyboard emulator class. It handles the key
presses from your USB keyboard and relaying those events to the Bluetooth
manager. The code for the keyboard emulator class does not require any
necessary changes from what Liam's tutorial presented, but I will reproduce it
here for completeness.

```python
    def __init__(self):
        self.state = [
                0xA1,   # This is an input report
                0x01,   # Usage report = keyboard
                # Bit array for Modifier keys
                [0,     # Right Super
                 0,     # Right ALT
                 0,     # Right Shift
                 0,     # Right Control
                 0,     # Left Super
                 0,     # Left ALT
                 0,     # Left Shift
                 0],    # Left Control
                0x00,   # Vendor reserved
                0x00,   # Rest is space for 6 keys
                0x00,
                0x00,
                0x00,
                0x00,
                0x00]

        # Get the local keyboard device
        while True:
            try:
                self.dev = evdev.InputDevice('/dev/input/event0')
                break
            except OSError:
                print("Keyboard not found, waiting 3 seconds before retrying")
                time.sleep(3)
        print("Found a keyboard")
```

In the constructor for the emulator, we setup the state of the keyboard and
grab the USB keyboard device. The state is represented as an array of bytes and
bits that maps to pressed keys.

```python
    def change_state(self, event):
        evdev_code = evdev.ecodes.KEY[event.code]
        modkey = keymap.modkey(evdev_code)
        if modkey > 0:
            # Toggle the modkey state
            active = self.state[2][modkey]
            self.state[2][modkey] = 0 if active else 1
        else:
            hex_key = keymap.convert(evdev_code)
            for i in range(4, 10):
                if self.state[i] == hex_key and event.value == 0:
                    self.state[i] = 0x00
                elif self.state[i] == 0x00 and event.value == 1:
                    self.state[i] = hex_key
                    break
```

The `change_state()` method takes a key press event, deciphers it, and change
the internal state accordingly. For a modifier key event, we toggle the
appropriate bit. For other keys, we either toggle the state for it or add it to
our input report.

```python
    def event_loop(self, bt_man):
        print('Listening for key events...')
        for event in self.dev.read_loop():
            if event.type == evdev.ecodes.EV_KEY and event.value < 2:
                self.change_state(event)
                bt_man.send_input(self.state)
```

Finally, the `event_loop()` method will continuously listen for key press
events and process them. The last piece of code we need is the main block that
setup the Bluetooth manager and keyboard emulator (Note that we need to run our
code as root in order to successfully bind to the Bluetooth connection ports we
defined earlier):

```python
if __name__ == "__main__":
    # Need to be root user to run
    if not os.getuid() == 0:
        sys.exit("Must be run as root user")

    bt_man = BluetoothManager()
    bt_man.listen()

    kb_emu = KeyboardEmulator()
    kb_emu.event_loop(bt_man)
```

We now have everything we need to start playing with the Bluetooth keyboard
emulator! On your Raspberry Pi, start up the emulator with the following
command and connect to it on your other devices:

```bash
$ sudo ./pitooth.py
```

## Navigating Android

A neat usage for the keyboard emulator is for controlling your Android device
via Bluetooth. Once you have your keyboard emulator up and running, it is easy
to connect to it on your Android device via the Bluetooth settings. The arrow
keys can be used for directional navigation, as expected. There is also a bunch
of keyboard shortcuts you can use within Android. Here are a couple interesting
shortcuts I found from playing with the keyboard emulator on Android
Marshmallow:

| Shortcut  | Description                  |
|-----------|------------------------------|
| Alt + Esc | Home                         |
| Esc       | Cancel or Back               |
| Space     | Unlock (only for swipe lock) |
| Super + N | Pull down Notifications      |

To reiterate the message from Liam's tutorial, this is a primitive Bluetooth
keyboard emulator and can obviously be extended with better features. For now,
the prototype works well, so go try it out with your Android or other devices!

[tutorial]: http://www.linuxuser.co.uk/tutorials/emulate-a-bluetooth-keyboard-with-the-raspberry-pi
[pybluez]: https://github.com/karulis/pybluez
[guide]: http://www.bluez.org/bluez-5-api-introduction-and-porting-guide/
[doc]: http://git.kernel.org/cgit/bluetooth/bluez.git/tree/doc/profile-api.txt
[test]: http://git.kernel.org/cgit/bluetooth/bluez.git/tree/test/test-profile
