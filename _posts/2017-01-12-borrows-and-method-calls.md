---
title: Borrows and Method Calls
tags: [rust]
---

Recently, I was picking Rust back up again after some time of reading and
following it. I started doing small exercises and re-implementing some
algorithms. It didn't take long until I hit an issue with the borrow checker.
It was an interesting issue with method calls when there is a mutable borrow,
and thought I should write about it here.  Coincidentally, my last post about
Rust was a year ago, which was also in January...

To describe the problem, it occurs when we invoke a function that takes a
mutable borrow of a variable as an argument, along with other arguments. The
method call part is when the other arguments are computed by calling a method
on the `mut`-borrowed variable. For example, imagine we have the following
function signature:

```rust
fn foo(arr: &mut [i32], idx: usize)
```

The function `foo` takes a mutable integer array and an index, and possibly
does something to the element at the given index. Imagine we want to call `foo`
on the last element. We will typically write it like so:

```rust
let mut xs = [1, 2, 3, 4, 5];
foo(&mut xs, xs.len() - 1);
```

We call `foo` by passing a mutable borrow of `xs` and the index value `4`. The
`xs.len()` is the method call. This feels intuitive and looks fine to me.
However, the compiler thinks otherwise:

```
error[E0502]: cannot borrow `xs` as immutable because it is also borrowed as mutable
 --> <anon>:3:18
  |
3 |     foo(&mut xs, xs.len() - 1);
  |              --  ^^          - mutable borrow ends here
  |              |   |
  |              |   immutable borrow occurs here
  |              mutable borrow occurs here

error: aborting due to previous error
```

Hm, I understand that we have a mutable borrow as the first argument, and that
`len()` takes an immutable borrow. But I was hoping the compiler will be smart
enough to figure out that we just need to call `len()` to compute the index.
After asking around on the `#rust-beginners` irc channel, a kind soul pointed
me to `nikomatsakis`'s [post][internals-post] about the problem.
Interestingly, it was posted on the same day I had encountered the issue. It
was a lengthy write-up and contained a lot of concepts and history that was out
of my scope, but I learned about the underlying problem (concerning how the
function call is desugared) and that a fixed will be pursued. Sweet.

In the meantime, we will just have to make do with temporaries, like so:

```rust
let mut xs = [1, 2, 3, 4, 5];
let idx = xs.len() - 1;
foo(&mut xs, idx);
```

We can also re-arrange the arguments so that the method call happens
first before the mutable borrow. Interestingly, this kind of gives away the
Left-to-Right evaluation order preference of the rust compiler.

[internals-post]: https://internals.rust-lang.org/t/accepting-nested-method-calls-with-an-mut-self-receiver/4588
