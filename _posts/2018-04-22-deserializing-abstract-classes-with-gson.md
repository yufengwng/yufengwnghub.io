---
title: Deserializing Abstract Classes with Gson
tags: [gson, java, serialization]
---

[Gson][gson-gh] is an excellent library for working with JSON in the Java
world. It works especially well when your data structures are simple and maps
one-to-one with java types. However, when you start to work with generics or if
the shape of your data can be ambiguous, then you will need to teach Gson some
new tricks in order to play nice with your specific data requirements. Let's
explore the situation when our input data can take on different shapes.

## Basic Usage with Gson

Imagine we work for a zoo and we need to handle data about animals. For
example, in our care might be dogs and cats, and we need to provide json data
about them to clients. We might represent that in our java application like so:

```java
abstract class Animal {
    // Name of this animal.
    protected String name;

    Animal(String name) {
        this.name = name;
    }
}

class Dog extends Animal {
    // Verbiage for when this dog barks.
    private String bark;

    public Dog(String name, String bark) {
        super(name);
        this.bark = bark;
    }
}

class Cat extends Animal {
    // Verbiage for when this cat meows.
    private String meow;

    public Cat(String name, String meow) {
        super(name);
        this.meow = meow;
    }
}
```

This is a straightforward representation where we have an abstract class for
animals that holds common data, and concrete subclasses that are particular to
each type of animal. The bit about the `bark` and `meow` verbiage is somewhat
contrived (this can be extracted out to a common `verbiage` field in the
base class) but we will stick with this as it helps with illustrating the
problem we will see later.

Alternatively, we can switch to using an interface for animals, but here we
will just focus on dealing with abstract classes. The technique discussed in
this post can be adapted to work with interfaces. Here's how we will use Gson
to serialize our data to json that can be consumed by clients:

```java
Gson serializer = new GsonBuilder()
        .serializeNulls()
        .setPrettyPrinting()
        .creat();
Dog foo = new Dog("bar", "baz");
String json = serializer.toJson(foo);
System.out.println(json);
```

The output will look like this:

```json
{
  "name": "bar",
  "bark": "baz"
}
```

That looks great! Now, let's feed that json back into our application and see
how we use Gson to deserialize it:

```java
Gson deserializer = new Gson();
Dog foo2 = deserializer.fromJson(json, Dog.class);

// foo2 is Dog{name='bar',bark='baz'}
```

That also looks good. So serialization and deserialization works well when we
know the concrete types. But what happens when we don't have the type at hand,
such as when we're dealing with abstract classes? With Gson, serialization will
work just fine since you will have to have concrete implementations of the
abstract classes, and Gson seems to be able to infer that and use that type
information. However, it poses a problem for Gson during deserialization (see
the part about "richer serialization semantics" in its [design
doc][gson-ddoc]).

## Illustrating the Deserialization Issue

To better illustrate the problem, let's imagine our application also provides
data about zookeepers that cares for animals:

```java
class ZooKeeper {
    private String name;    // Their name.
    private String zoo;     // Name of the zoo they work at.
    private Animal buddy;   // The animal they care for at the zoo.

    public ZooKeeper(String name, String zoo, Animal buddy) {
        this.name = name;
        this.zoo = zoo;
        this.buddy = buddy;
    }
}
```

And let's use Gson to work with this new data:

```java
ZooKeeper bob = new ZooKeeper("bob", "safari", new Dog("doge", "bark bark"));

String json = serializer.toJson(bob);
System.out.println(json);

ZooKeeper bob2 = deserializer.fromJson(json, ZooKeeper.class);
```

The serialization part will output the following, which looks fine:

```json
{
  "name": "bob",
  "zoo": "safari",
  "buddy": {
    "name": "doge",
    "bark": "bark bark"
  }
}
```

But the deserialization part will fail with this:

```text
java.lang.RuntimeException: Unable to invoke no-args constructor for class ...
    ...
Caused by: java.lang.UnsupportedOperationException: Abstract class can't be instantiated! Class name: ...
    ...
```

This is an inherit issue that stems from java's type system limitations and the
mismatch between java and json. When Gson sees the `Animal buddy` field in
`ZooKeeper`, it tries to deserialize that part of the json input as such, but
it does not have information about which concrete subclass to use.

## Addressing the Deserialization Issue

To address this problem, we will add a "lookahead" ability to determine the
concrete subclass type to use, and teach Gson how to use this new ability.

First, we will add a new property to our json that helps disambiguate the type
of animal we're dealing with, and thus make certain the shape of the data. Our
new json schema looks like this:

```json
{
  ...
  "buddy": {
    "type": "DOG",
    "name": "doge",
    "bark": "bark bark"
  }
}
```

Aside from a `type` string property, we can also use something else, as long as
it helps us determine the type of thing we're looking at. This also has the
added benefit of making it much easier and clearer for clients consuming the
json data; clients can rely on the `type` property instead of other heuristics
to know what type of animal object they're dealing with. It's a win-win move.

Next, we will support this in our java application using an `enum`:

```java
enum AnimalType {
    CAT,
    DOG,
}

abstract class Animal {
    protected AnimalType type;
    ...
    Animal(AnimalType type, String name) {
        this.type = type;
        ...
    }
}

class Dog extends Animal {
    ...
    public Dog(String name, String bark) {
        super(AnimalType.DOG, name);
        ...
    }
}

class Cat extends Animal {
    ...
    public Cat(String name, String meow) {
        super(AnimalType.CAT, name);
        ...
    }
}
```

Then, we need to implement custom deserialization for `Animal`s that
essentially "looks ahead" and use the new `type` property. This part requires
more thorough explanation, so bear with me for a few moments. From Gson's [user
guide][gson-guide], we learn that there are a few ways to customize Gson's
deserialization behavior. More precisely, we can:

1. Use Gson's low-level `JsonParser` to inspect the animal json object, find
   the `type` property, and then invoke `Gson.fromJson()` with the
   corresponding `Animal` subtype. The DOM style of the `JsonParser` API seems
   pretty nice to work with. However, since we start the deserialization
   process at `ZooKeeper`, we need some way to "inject" our code into Gson in
   order to get it to run our stuff when it sees an `Animal`. Using this
   approach alone is not enough.

   <p/>

2. Register a [type adapter][gson-tadapter] for `Animal`. This can serve as the
   injection mechanism we need for approach (1) above. My concern with this
   approach is that the type adapter uses the `JsonReader` streaming style API.
   The streaming API seems pleasant when you have simple data, but not so when
   it's an array or object. Since an animal comes in as a json object, this
   will mean we need to use `JsonReader.beginObject()`, and
   `JsonReader.endObject()`, and handle all the properties in-between. It
   basically means we will have to implement parsing the animal object
   ourselves when we can easily ask Gson to do it for us if we had used the DOM
   style API.

   <p/>

3. Register a [custom deserializer][gson-deser] for `Animal`. This is akin to
   approach (2) above. But instead of using the streaming API of `JsonReader`,
   it uses the DOM API of `JsonParser`. The `deserialize()` method we need to
   implement as part of this approach receives a `JsonElement` that represents
   the incoming animal object, already parsed into a json tree and ready to be
   used with `Gson.fromJson()`. This approach, in my opinion, gives us best of
   both worlds and allows us to solve the problem at hand.

To apply approach (3), we will implement it like so:

```java
class AnimalDeserializer implements JsonDeserializer<Animal> {
    @Override
    public Animal deserialize(JsonElement elem, Type t, JsonDeserializationContext ctx) throws JsonParseException {
        JsonObject json = elem.getAsJsonObject();
        AnimalType type = AnimalType.valueOf(json.getAsJsonPrimitive("type").getAsString());
        switch (type) {
            case CAT:
                return ctx.deserialize(json, Cat.class);
            case DOG:
                return ctx.deserialize(json, Dog.class);
            default:
                throw new JsonParseException("unhandled animal type " + type);
        }
    }
}
```

We look for the `type` property in the animal object, and map that to the
concrete subclasses. Since Gson knows how to deserialize to the concrete
classes already, we ask it to do so using the `ctx` that was passed in.
Obviously, the real thing should have more robust error handling. Imagine a
client giving us `"type": "FISH"` in the json...

I like to think of the lookahead mentioned here as a similar technique to the
lookahead in parsers. If you know how compilers work, the parser piece uses
token lookahead in order to properly scan for lexical tokens. If you see a `/`
character, you need to do a lookahead and peek at the next character in order
to decide if it is a `//` token or a `/*` token.

Lastly, we tell Gson about our custom deserializer by registering it:

```java
Gson deserializer = new GsonBuilder()
        .registerTypeAdapter(Animal.class, new AnimalDeserializer())
        .create();
...
ZooKeeper bob2 = deserializer.fromJson(json, ZooKeeper.class);

// bob2 is ZooKeep(name='bob',zoo='safari',buddy=Dog{type=DOG,name='doge',bark='bark bark'}}
```

Yay! We can now serialize and deserialize abstract classes using Gson!

## Default Serialization, Custom Deserialization

Throughout our exploration, we have used _separate_ instances of Gson for
serialization and deserialization. This is an important note I want to point
out and emphasize. If you use the same Gson instance for both, you will see
some funny json outputs. For example, let's use this Gson instance:

```java
Gson gson = new GsonBuilder()
        .registerTypeAdapter(Animal.class, new AnimalDeserializer())
        .serializeNulls()
        .setPrettyPrinting()
        .create();
```

Using this `gson` instance, you will notice this serialized json output:

```json
{
  "name": "bob",
  "zoo": "safari",
  "buddy": {
    "type": "DOG",
    "name": "doge"
  }
}
```

Yup, the `bark` field is completely ignored! Grokking through the code, it
seems like registering _any_ kind of adapter for the base class will bias Gson
to prefer an adapter for the base class over the actual class we're dealing
with. Obviously we didn't register a serializer adapter for the base class
`Animal`, but it still prefers the base class and falls back to some standard
adapter for it. In anycase, when you want the default serialization with some
custom deserialization, prefer to use two different Gson instances.

And that's a wrap! Go forth and Gson some json!

[gson-gh]: https://github.com/google/gson
[gson-ddoc]: https://github.com/google/gson/blob/master/GsonDesignDocument.md
[gson-guide]: https://github.com/google/gson/blob/master/UserGuide.md
[gson-tadapter]: https://static.javadoc.io/com.google.code.gson/gson/2.8.2/com/google/gson/TypeAdapter.html
[gson-deser]: https://static.javadoc.io/com.google.code.gson/gson/2.8.2/com/google/gson/JsonDeserializer.html
