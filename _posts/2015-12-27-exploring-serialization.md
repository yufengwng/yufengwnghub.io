---
title: Exploring Serialization
tags: [ruby, serialization]
---

Many programs need to store or retrieve data at some point. Other programs will
need to translate data to a compatible format when communicating with other
programs. One good example is web applications. The client and server need to
communicate in a certain format. On the server-side, it needs to store the data
in a certain format so it can retrieve it later. One technique to accomplish
these needs is serialization. You can think of serialize/deserialize as
storing/restoring data, respectively. We serialize data into a storage/transfer
format. And we deserialize the stored data to restore our original data.

## JSON Serialization

There are several serialization standards one can use depending on your needs,
such as XML and CSV. Currently, JSON (JavaScript Object Notation) is a very
popular and versatile standard format to use. The syntax for JSON stems from
JavaScript, but the datatypes supported are highly universal to other
languages. For example, Ruby supports JSON and it comes in the standard
installation since Ruby version 1.9.3. In regards to web applications and
RESTful APIs, JSON seems to be the standard data format, so there should be
good support no matter what language or framework you use.

Using Ruby as an example, let's take a look at how we can serialize to JSON.
The standard datatypes in Ruby map nicely to those supported in JSON. Numbers,
strings, booleans, and arrays map to their corresponding datatype in JSON. On
the other hand, hashes in Ruby will map to JSON objects.

```ruby
>> require 'json'
>> 25.to_json
=> "25"
>> 180.25.to_json
=> "180.25"
>> true.to_json
=> "true"
>> "my string".to_json
=> "\"my string\""
>> [1, 2, 3].to_json
=> "[1, 2, 3]"
>> { :a => 1, :b => 2, :c => 3 }.to_json
=> "{\"a\":1,\"b\":2,\"c\":3}"
```

In the above snippet, we see how Ruby datatypes map to JSON supported
datatypes. One thing of note is that although JSON support comes standard with
Ruby, we still have to include it using a `require`. Another note is that
symbols in Ruby gets mapped to strings in JSON, since JSON syntax dictate that
keys in objects are strings.

### Serialize to JSON

Instead of calling the `to_json` method to serialize the data, another way is
to utilize the methods in the Ruby `JSON` module. We can pass a Ruby object to
the `JSON.dump` method and it will serialize the data into JSON.

```ruby
>> require 'json'
>> h = { :a => 1, :b => false, :c => "str" }
>> JSON.dump h
=> "{\"a\":1,\"b\":false,\"c\":\"str\"}"
```

### Deserialize from JSON

Similarly, we can use the `JSON` module to deserialize data into Ruby objects.
We can provide a JSON string to the `JSON.load` method, and we will get back a
Ruby hash with the data in it.

```ruby
>> require 'json'
>> data = "{\"a\":true,\"b\":[1,2,3],\"c\":\"my string\"}"
>> JSON.load data
=> { "a" => true, "b" => [1, 2, 3], "c" => "my string" }
```

## YAML Serialization

Another popular serialization format, especially in the Ruby community, is
YAML. This data exchange standard is designed to be human-readable, and as of
version 1.2 a superset of JSON. A common usage of YAML is for specifying
configuration. The below snippet is an example of what YAML looks like. The
syntax is very easy to pick up, and there are many features that JSON does not
have, such as comments. Occasionally, you might see a line with triple dashes
(`---`) in a YAML file. This is a YAML document separator, and is used at the
beginning of a document record.

```yaml
---
# this is a comment
a: 25
b: true
c:
  - "YAML"
  - "Ain't"
  - "Markup"
  - "Language"
d:
  name: "yaml"
  version: 1.2
```

### Serialize to YAML

Similar to JSON, Ruby has built-in support for YAML serialization and
deserialization. We can use the `YAML.dump` method to serialize data, as seen
in the snippet below.

```ruby
>> require 'yaml'
>> h = { :a => 25, :b => true, :c => [1, 2, 3], :d => "str" }
>> YAML.dump h
=> "---\n:a: 25\n:b: true\n:c:\n- 1\n- 2\n 3\n:d: str\n"
```

Again, we have to bring in the YAML module by doing a `require`. If we look
closely at the output from `YAML.dump`, we see that symbols get serialized to
YAML strings that are prepended with a colon.

### Deserialize from YAML

Likewise, if we want to deserialize data in YAML format, we can use the method
`YAML.load`. As an example, we will read in the configuration file we saw
earlier and deserialize the data.

```ruby
>> require 'yaml'
>> File.open("config.yaml", "r") do |f|
>>   YAML.load f.read
>> end
=> {"a"=>25, "b"=>true, "c"=>["YAML", "Ain't", "Markup", "Language"], "d"=>{"name"=>"yaml", "version"=>1.2}}
```

## Binary Serialization

Another technique for data serialization is to use the good old binary format.
This format is not universal like JSON or YAML, but tends to be faster when
serializing or deserializing. Binary serialization comes standard in Ruby, and
does not need a `require` like for JSON. In Ruby, the `Marshal` module provides
the support to serialize and deserialize data structures. Keep in mind that you
will want to use the same version of `Marshal` to serialize and deserialize
your data.

### Serialize to Byte Stream

Like the `JSON` and `YAML` modules, the `Marshal` module has a `dump` method
that will serialize the data into a byte stream. Using Ruby version 2.2.1p85 as
an example, here is what serializing will look like.

```ruby
>> h = { :a => 25, :b => true, :c => "str" }
>> Marshal.dump h
=> "\x04\b{\b:\x06ai\x1E:\x06bT:\x06cI\"\bstr\x06\x06ET"
```

### Deserialize from Byte Stream

In reverse, if we have the byte stream for some data, we can use the
`Marshal.load` method to deserialize the data.

```ruby
>> data = "\x04\b{\b:\x06ai\x1E:\x06bT:\x06cI\"\bstr\x06\x06ET"
>> Marshal.load data
=> { :a => 25, :b => true, :c => "str" }
```

## Marshalling Objects

When dealing with serialization, you might come across the term marshalling. In
the object-oriented programming world, the word marshalling defines the process
of serializing objects. It seems fitting that the module that serialize objects
in Ruby is named `Marshal`.

We have already seen how to marshal objects to the JSON format in the previous
sections. If we are just dealing with simple Ruby objects like arrays and
hashes, then we can continue to use the `dump` and `load` methods from the
`JSON` module. This is usually the case when we interact with RESTful services
that provide JSON. However, if we have custom objects and want to serialize to
JSON, we will need to write our own `to_json` methods. On the other hand, the
`Marshal` module will be able to serialize and deserialize the state of our
custom objects in binary format.

In summary, there are many serialization formats available for data exchange
and storage. Depending on what projects or environments you work in, you might
choose one standard over the others. Each standard comes with its own set of
features and peculiarities, so dive into their respective documentations to
find out more.

