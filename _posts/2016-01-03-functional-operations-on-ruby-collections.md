---
title: Functional Operations on Ruby Collections
tags: [fp, ruby]
---

Arrays and Hashes are very common and versatile data structures provided in
Ruby. Arrays are a one-size-fits-all data structure that can be used as a
container for a list of things, and can be adapted to be used as a stack or
queue. On the other hand, Hashes allow us to create mappings between a key and
a value. Ruby provides many functionalities for these two data structures, such
as adding and deleting elements, accessing elements, and sorting elements.
Often times, however, we need to perform complex or custom operations on these
collections. One simple example is converting all the strings in an array
collection to uppercase. An initial approach in Ruby is to start with a new
array, then iterate over the collection elements and adding their uppercase to
the new array.

```ruby
animals = %w(cat crab snail monkey snake)

upcases = []
animals.each { |element| upcases << element.upcase }

upcases #=> ["CAT", "CRAB", "SNAIL", "MONKEY", "SNAKE"]
```

Turns out Ruby actually has an abstraction for this called the `map` method.
You can call the `map` method on an array collection and provide a block that
handles the transformation you need. For the animals example, we no longer need
to explicitly create a new array since the `map` method will return to us a new
array with the transformed elements.

```ruby
animals = %w(cat crab snail monkey snake)

animals.map { |element| element.upcase }
#=> ["CAT", "CRAB", "SNAIL", "MONKEY", "SNAKE"]
```

In the functional programming world, map transformation is a common and
rudimentary technique, along with filtering and folding. Although Ruby is an
Object-Oriented language, the Ruby collections provide methods that allow us to
simulate functional operations like mapping, filtering, and folding. The method
names are intuitive, like `map` and `reduce`, but we will be looking at the
\*ect family of methods. These methods allow us to perform functional
operations on collections and their names all end with "ect".

## Mapping

In my opinion, the map transformation concept is very straightforward to
understand. We have a collection of things, and we want to perform some
operation on each of them. The resulting collection we get back will contain
the results of each of those operations. The `collect` method allows us to do
just that. It takes no arguments, but expects a block. `collect` will invoke
the block for each element in the collection, yielding the element to the
block.

### `Array#collect`

Let's take a look at the `collect` method for Arrays. We will see some usages
using the animals example from before, and then we will conclude it with a note
on the general usage.

```ruby
# reverse each animal name
animals.collect { |element| element.reverse }
#=> ["tac", "barc", "lians", "yeknom", "ekans"]

# capitalize and add a prefix to each animal name
animals.collect { |element| element.capitalize.prepend "A-" }
#=> ["A-Cat", "A-Crab", "A-Snail", "A-Monkey", "A-Snake"]

# length of each animal name
animals.collect { |element| element.length }
#=> [3, 4, 5, 6, 5]

# general usage
array.collect do |element|
  # transformation on element
end
```

### `Hash#collect`

Using map transformation on a hash is not as straightforward as
`Array#collect`, since we are working with key-value pairs. Hash collections
provide us the same `collect` method, but the difference is in the block
arguments. The `Hash#collect` method yields to the block a two-element array
containing the key and the value of that pair. Also note that the `collect`
method will return a new array and not a new hash. To get a new hash, one way
is to use `Hash#reduce` or `Hash#inject`, which we will take a look at in a
later section.

As an example, let us give each animal a rating between 1 and 10. Using that
hash collection, we can see what the two-element arrays will be and the
resulting array.

```ruby
ratings = { :cat => 10, :crab => 9, :snail => 7, :monkey => 8, :snake => 3 }

ratings.collect { |pair| pair }
#=> [[:cat, 10], [:crab, 9], [:snail, 7], [:monkey, 8], [:snake, 3]]
```

We can see from the above example that we will end up with an associative array
with all the key-value pairs. In our block to `collect`, we can work with the
`pair` array, using an index to pick out the key and the value individually.
However, often times we will want to work with the key and value separately
without indexing into an array. Instead of taking one argument in the block, we
can take two arguments. This will destructure the two-element array into a key
argument and a value argument. Let's take a look at how we can do this, along
with some examples and the general use for `Hash#collect`.

```ruby
# using key-value arguments in block
ratings.collect { |key, value| {key=>value} }
#=> [{:cat=>10}, {:crab=>9}, {:snail=>7}, {:monkey=>8}, {:snake=>3}]

# get length of keys
# Note: the value argument is not used
ratings.collect { |key, _| key.length }
#=> [3, 4, 5, 6, 5]

# multiply animal rating by 10
# Note: the key argument is not used
ratings.collect { |_, value| value * 10 }
#=> [100, 90, 70, 80, 30]

# general usage
hash.collect do |key, value|
  # transformation using key and/or value
  # block return value collected in array
end
```

The name for the `collect` method makes a lot of sense since all the results
are "collected" into a new array. The `map` method is essentially an alias for
`collect`. Personally, I will prefer to use `map`, but if you are using the
other functional operations, it might be nice to use `collect` since it follows
the same naming convention as the others.

## Filtering

Once you understand the map transformation concept, filtering will be fairly
easy to pick up as well. Filtering is very similar to mapping, instead of
transformations we work with predicates. This means we ask questions about the
elements, and the block must return a boolean value. The element will only be
added to the resulting array if the block returns true (i.e. not `false` or
`nil`).

### `Array#select`

Like `collect`, each element is yielded to the block for array collections.
Let's continue with the animals example.

```ruby
# pick only animals with names greater than 4 characters
animals.select { |element| element.length > 4 }
#=> ["snail", "monkey", "snake"]

# pick only animal names that start with 'c'
animals.select { |element| element.start_with? 'c' }
#=> ["cat", "crab"]

# general usage
array.select do |element|
  # filter base on element
  # block should return boolean
end
```

### `Hash#select`

For hash collections, the `select` method differs from `collect` in significant
ways. First, the `select` method will yield to our block the key and value
separately instead of yielding a two-element array. This means if you write
your block to `select` with only one argument, you will only get the key.
Second, the `select` method actually returns a new hash instead of an array.
Let's continue with our ratings example.

```ruby
# pick only ones with length of key greater than 4 characters
ratings.select { |key, _| key.length > 4 }
#=> { :snail => 7, :monkey => 8, :snake => 3 }

# pick only ones with a rating greater than 5
ratings.select { |_, value| value > 5 }
#=> { :cat => 10, :crab => 9, :snail => 7, :monkey => 8 }

# general usage
hash.select do |key, value|
  # filter base on key and/or value
  # block should return boolean
end
```

The name for `select` also makes a lot of sense, since we are "selecting"
elements based on some criteria. Unfortunately, there is no method named
`filter`. On the other hand, there is a method named `reject`, which is the
opposite of `select`. This method will not add elements to the resulting array
if the block returns true.

## Folding

The folding concept can be more difficult to grasp than mapping or filtering.
"Fold" is a common term you will see in the functional programming world, but
other times you might see the term "reduce". The idea of fold, or reduce, is to
take a collection of things and get a single result from that. The data set
gets smaller, like if you are folding a piece of paper into a smaller shape.
And you end up with one result, like if you are folding multiple lines in vim
into a single line.

The best and most common example is finding the sum of an array of numbers. If
you were to do this by hand, it will go something like this:

1. Start with 0 as the sum.
2. Start with first number.
3. Add number to sum.
4. Remember the partial sum.
5. Move to next number.
6. Repeat steps 3 - 5 until we finish with the last number.
7. The partial sum is now the total sum.

From that example, we can pick out three key concepts:

* Start with an initial value.
* Have some form of accumulation.
* Iterate through all the elements.

For the summing example, the initial value was 0, and the accumulation was the
sum itself. For array and hash collections, the `inject` method allows us to
perform folding. The `inject` method takes an optional argument, and expects a
block. The optional argument is for the initial value. If it is not provided,
then the first element in the collection will become the initial value. The
`inject` method will yield two values to the block, one being the accumulation
value, and the other being the element. And the return value from the block
becomes the new accumulation value.

### `Array#inject`

It is best to look at examples to understand folding. We will use the animals
example for `Array#inject` with `memo` as our accumulation value. The resulting
value from `inject` will be the last value of `memo`.

```ruby
# total length of all animal names
animals.inject(0) { |memo, element| memo + element.length }
#=> 23

# concatenate the capitalize letter of animal names
animals.inject("") { |memo, element| memo << element.capitalize[0] }
#=> "CCSMS"

# concatenate the animal names
animals.inject { |memo, element| memo + element }
#=> "catcrabsnailmonkeysnake"

# general usage
array.inject(optional_initial_value) do |memo, element|
  # perform accumulation base on element
  # memo represents current accumulation value
  # block return value becomes new memo
end
```

Notice that in the third example we are using `String#+` instead of
`String#<<`. The `+` method creates a new String object, while `<<` mutates
the String object itself. Since we did not specify an initial value, `memo` is
initially the first element. Thus, we have to be careful not to mutate the
element itself as part of accumulation. In general, you will want to specify an
initial value for folding.

### `Hash#inject`

The `inject` method for hash collections has the same characteristics as the
one for arrays, but with one exception. For each element that `Hash#inject`
yields to the block, it yields a two-element array just like `Hash#collect`.
Like for `Hash#collect` we can destructure the two-element array into a key
argument and a value argument. We will need to put the key and value block
arguments in parenthesis to ensure destructuring happens.

```ruby
# using key-value block arguments and no initial value
ratings.inject { |memo, (key, value)| memo << key; memo << value }
#=> [:cat, 10, :crab, 9, :snail, 7, :monkey, 8, :snake, 3]

# sum up all the ratings
ratings.inject(0) { |memo, (_, value)| memo + value }
#=> 37

# sum up the length of the keys
ratings.inject(0) { |memo, (key, _)| memo + key.length }
#=> 23

# decrease all the ratings by one
ratings.inject({}) { |memo, (key, value)| memo[key] = value - 1; memo }
#=> { :cat => 9, :crab => 8, :snail => 6, :monkey => 7, :snake => 2 }

# general usage
hash.inject(optional_initial_value) do |memo, (key, value)|
  # perform accumulation base on key and/or value
  # memo represents current accumulation value
  # block return value becomes new memo
end
```

We can see from the fourth example that the result is a new hash. Using the
`inject` method and an empty hash as the initial value, we can perform map
transformation on each element and get back a new hash of those transformation
results.

I'm not sure what analogy will make the most sense for the name `inject`.
Perhaps you can think of performing operations on each element, and "injecting"
the results into the accumulation. To that end, I will personally prefer to use
the `reduce` method instead, which is just an alias for `inject`.

## Tips and Tricks

You might have realized from the fold examples that those can be easily
achieved using other functional operations or regular collection methods. The
beauty of fold is that you can implement the other functional operations, like
map and filter, using just fold. Here is a quick example of using `reduce` to
simulate mapping animal names to the name lengths. We start with an empty
array, and then add the lengths as we iterate through the animals array.

```ruby
# simulate mapping using only folding
animals.reduce([]) { |memo, e| memo << e.length }
#=> [3, 4, 5, 6, 5]
```

Another trick is to use method symbols instead of giving a block, when
applicable. If the transformation or filtering that you need can be done with a
method call, then you can pass that method symbol as an argument instead of
writing a block. Here is a quick example where we find the total length of the
animal names. We first map the names to lengths, then sum up the lengths.

```ruby
# using blocks
animals.map { |e| e.length }.reduce(0) { |memo, e| memo + e }
#=> 23

# using method symbols
animals.map(&:length).reduce(0, :+)
#=> 23
```

Ruby provides a set of sensible and rich APIs for operating on collections.
Many of the functionalities come from the `Enumerable` module, so that is a
great place to start if you want more details. By chaining and combining these
functional operations and other methods, you can perform much more complex and
powerful processing operations.

