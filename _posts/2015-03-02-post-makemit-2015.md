---
title: Post MakeMIT 2015
tags: [hackathon, makemit]
---

It was early 7 AM and the air outside was beyond chilly here in Boston. The sky
was clear, the streets deserted, and the parking garage pretty much empty. I
got in the car with three other friends, and we set out to Lobdell Hall at MIT
for MakeMIT 2015. It was just a straight drive from the Northeastern University
campus via Massachusetts Avenue. We pulled into the parking garage, and set
foot on the MIT campus. Not quite sure which direction to head towards, we
started walking towards a building with a food court and Dunkin Donuts. We
might have been intuitively drawn towards the faint scent of morning coffee.
Sure enough, the building was indeed Lobdell Hall, the venue for the hardware
hackathon drawing 50+ teams, a total of over 300 makers and hackers.

Hackathon events are pretty famous and widspread among developers by now.
There's a huge [community](https://mlh.io) around this hackathon idea, and
events are popping up everywhere on the map. I'm personally excited about these
events where you get to hack on an idea and make some new hacker friends. But
[MakeMIT](http://makemit.org) was different. It was the first one I heard of
that focuses on hardware hacks. I had missed out on previous hackathon events,
so by chance, MakeMIT became the first hackathon I would attend, and at that
the first hardware hackathon that I attended.

I went into MakeMIT with three other friends. We didn't go in with a novel idea
in mind, but we still wanted to attend for the experience and to take a chance.
We wanted to make a smart speaker that will automatically adjust the volume as
you roam, so you can enjoy your music as you move around the room. For a rough
prototype, We ended up deciding to use the [Intel Edison][edison] as the main
control for our project, the Kinect for sensing the user, and the
[Myo armband][myo] for user controls. We had no idea if it was going to work at
all, and it was our first time playing with these devices and platforms. We
ultimately ran into lots of trouble setting up our development environments,
and a lot of issues trying to connect these devices together.

I worked on the Intel Edison and getting it to work with the other devices. I
was able to hook it up to my machine and got it running. I started first
checking out the documentation and guides. Then I went ahead to flash the
firmware for the device. This was required because, as I learned from the Intel
representatives there, the wifi module was somehow missing, so we had to get
the latest firmware. The amazing thing about the Edison was the build-in
support for wifi. And to my surprise, the Edison actually runs a lightweight
version of Linux called Yocto!

Quite honestly, I had relatively no idea how to work with the Intel Edison. The
last time I played with a prototyping board and hardware was roughly 2 years
ago with the Arduino Uno. Back then, the boards were small, features were
limited, and you code in a slight variant of C. But with the Edison, you get
Linux, you can install packages, and you can run Arduino sketches, Node.js, and
python. I remembered a few things from Arduino, like gpio and pwm pins, so I
was able to quickly work through some of the hello world examples, like the
famous LED blinking example. I wasn't exactly sure how we can connect the
different devices together, but I did know about web technologies, REST, and
web services. So to take advantage of the Edison's wifi capability, I suggested
that we run a server on the Edison and expose a web API that the other devices
can talk to. Everything was going well until we hit wifi issues with MIT's
wireless guest network. I couldn't get on the network. I opened up a hotspot to
put my machine and the Edison on the same network for some time, until the
MakeMIT team asked us to turn off the hotspot to relieve congestion. So for
some time, I couldn't test out my work on the Edison. Fortunately, the MakeMIT
team got the wifi going again. By the end of this, I got a Node server running
on the Edison with a web API ready to go.

Another thing I helped with was getting the speaker part working, or at least I
tried to. We were going to use a sound shield that can read music files and
play them out to a speaker. We weren't sure if the Arduino Wave Shield will
work well with the Edison, but the Edison breakout board has the pins for it,
so we went ahead to try it. One of my teammates took the kit and soldered
everything together for the shield. We had a sample music file loaded on an SD
card and ready to go, and we just need to get the Arduino sketch program for
it. This is where we ran into issues. I got the WaveHC library imported, but I
couldn't get the sample sketches compiled. I thought it was a problem with the
Intel version of the Arduino IDE I was using, but I ran into the same issues
using the normal Arduino IDE, so it seemed like an issue with the library
itself. This was a crucial part of the system, but we couldn't get it working.
Without the speaker, I needed another way to demonstrate that the web API was
working. Hence, I had an LED connected to the Edison, and you can control it
via the web API.

In the end, we ran into lots of issues working with the different devices. It
was our first time being exposed to these technologies, and we spent valuable
time trying to learn about them and getting them to work in certain ways. We
had bits and pieces of the system, but nothing was put together at the end of
the day. My team and I was not successful in our first hackathon. However, it
was a phenomenal experience, and I learned a ton of new things. I reunited with
prototype development boards, and was exposed to the advancements in these
technologies since my first encounter two years ago. The Intel Edison itself
was already amazing, but there's also platforms that are built solely for
Node.js and JavaScript, like the [Tessel](https://tessel.io) and the
[Kinoma Create](http://kinoma.com). The Myo armband was also fascinating to
see in real action. There were a lot more really cool technologies and hardware
at the event. It was probably the longest event I had attended so far, and I
was definitely super tired near the end of it.

Aside from all the hacking, it was great meeting some of the people there. We
actually met an engineer from a startup called [Pavlok](http://pavlok.com).
When he asked if we have heard of Pavlok, one of my teammates and I cracked up
laughing. Incidentally, we have a friend who actually worked at Pavlok as an
intern not too long ago. The Pavlok engineer was showing us the Pavlok bracelet
and the technology around that. We had a pretty fun conversation with him. At
another time later on, an engineer from [Levant](http://levantpower.com) came
up to us, and my team immediately kicked off a conversation with him. The
Levant engineer was actually a recent Northeastern graduate, so we had plenty
to talk about. To make matters even better and to our surprise, we found out
that he is actually one of my teammate's coworker's boyfriend. That sounds like
a distant and unfamiliar relation, but it brought the conversation down to a
more personal level.

I must now take the next moments to talk more about the MakeMIT event itself
and the MakeMIT team, because they were awesome. As I came to learn, this was
the second year they were having this hackathon. The MakeMIT team did a great
job organizing and communicating with us prior to the event. At the event, the
moderator was a clear and eloquent speaker. I really applaude the MakeMIT team
for setting up the online system for the event, where we could request for
parts, request for mentoring help, find tutorials and other resources. There
was one or two hiccups, but the online portal was neat and helpful. Throughout
the day, you will see the team of 11 people running around, making prints and
laser cuts for teams, and answering questions at the Help Desk. When the MIT
guest wifi issue emerged, the MakeMIT team promptly responded, and expertly
tried to control the situation. You could really feel the close teamwork and
immense effort of the team.

The venue for the event was at one side of Lobdell Hall, taking up roughly two
floors. I have actually been to the venue before for another event a few years
ago, so I was faintly familiar with the venue layout. The place was packed with
makers, hackers, and sponsors. Everyone was really excited and really focused.
However, it seemed like there was a lot less people than when we started as we
neared the end of the event. I noticed some tables were emptied out, so we
concluded that some teams left and went home. Although my team was not doing
well, I was very grateful that we stayed for the whole thing. It was really
cool going around and seeing other people's hacks during the demo session. Some
of the ideas were not distinctively novel, but they were impressive
alternatives and used better/cheaper techniques. The top ten teams were invited
back by the MakeMIT team to further development their ideas and products.

I went into MakeMIT 2015 with nothing, and I came out with a lot of exposure to
new technologies, techniques, and ideas. I showed up, I participated, and I
learned. By and by, I think that was the more important part.

[edison]: http://www.intel.com/content/www/us/en/do-it-yourself/edison.html
[myo]: https://www.thalmic.com/en/myo
