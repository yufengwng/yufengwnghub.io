---
title: Where the Sound Comes From
tags: [audio, headphones]
data:
  banner: /assets/img/blog/headphone_m50x.jpg
---

Music is a phenomenal medium. It lets you express yourself, your emotions, your
ideas, your worries, your stories, and a whole lot more. Despite the language
differences, music itself is universally understood, and anyone can pick up and
produce music. I enjoy listening to music. It's especially more energizing and
engaging if you had danced for a few years. I don't always have a chunk of time
to just listen to music, but when I do I definitely want to have high quality
equipment for it.

I have always used cheap and low-grade in-ear headphones, or earphones. Those
work well and sounds acceptable, plus it costs way less to get a pair.
Recently, I managed to shell out enough money to get an actual pair of
headphones. I did quite a bit of research, and the [Audio-Technica ATH-M50X][1]
seemed to be overall the best option for me. It performs well, and has a fair
price tag. During my research, I also collected a few notes on headphones in
general. I want to share these notes about where the sound actually comes from.

### Audio Drivers

Sound is produced from vibrations in a fluid medium. Every piece of audio
equipment, including earbuds and headphones, produce sound using a component
called the driver. This driver device is made of magnets, electrical wires, and
a few other delicate materials.

![Headphone parts](/assets/img/blog/headphone_components.jpg
"General components that make up headphones")

As we can see in the above image, the driver is one of the components that make
up the headphone. It is enclosed within some variation of frames and casing,
and connected to the audio wire. Of course, the above image is not drawn to
scale, nor is it drawn to be an accurate depiction of the M50X. I have not
taken apart my headphones, and I don't ever plan to, so I will not know what
exact components it has inside.

For headphones that convert electrical audio signals to sound, the driver is
responsible for creating the vibrations in the air to produce those sounds. The
motion that "drives" these vibrations are not directly caused by electrical
energy, but rather as a result of magnetic force interactions. However,
electrical energy is needed to produce some of the required magnetic forces.

The most common type of audio drivers is the dynamic driver. You will typically
find [dynamic drivers][2] in headphones and also earbuds. In fact, the M50X has
a custom 45 mm dynamic driver.

![Dynamic driver](/assets/img/blog/headphone_dynamic_driver.jpg
"Inner workings of a dynamic driver")

The above image shows how a dynamic driver typically looks like from the top,
and also the cross section of such a driver. Again, the image is not drawn to
scale or accuracy. In the top view, we are looking down at the diaphragm. This
piece is a thin plastic that moves the air to create the sound vibrations. The
spiral slices we see on the diaphragm, which makes it look like a seashell, are
folds on the diaphragm to make it easier for the diaphragm to move. The cross
section is a cut at the diaphragm and the circular magnet.

The science behind the motion in the dynamic driver involves magnetic magic.
When electrical current runs through a conducting wire, it creates a magnetic
field. The direction that this magnetic field rotates around the wire is
dictated by the so called "right-hand rule." This rule specifies that if you
wrap your right hand around the wire with your thumb pointing along the
direction that the current is traveling, then the magnetic field rotates in the
same direction as the direction your other fingers curl around the wire.

When the magnetic field of the current interacts with another magnetic field,
it creates motion. The direction of this motion is dictated by the so called
"left-hand rule." This rule instructs you to stick your thumb, index finger,
and middle finger along the three axes. The middle finger will follow the
direction of current flow, the index finger will follow the direction of the
magnetic field of the circular magnet, and the thumb will indicate the
direction of movement.

In order to get the current's generated magnetic field to interact with the
magnetic field of the circular magnet, we cut out a gap in the magnet. Within
this gap, we suspend the electrical wire. This wire is sometimes called the
"voice coil" since we pass the audio signal, which is essentially electrical
signal, through this wire and it is usually coiled in several windings. Since
the voice coil is arranged in a loop, we made the magnet circular so the voice
coil can be uniformly suspended within the magnetic gap. We then attach the
voice coil to the diaphragm, so we get the actual diaphragm to move.

The diaphragm needs to be able to move upwards and downwards. As we can see
from the vector diagram in the previous image, the diaphragm will move up when
current flows out of the page and the magnetic flux flows from the outer to
inner magnet. Similarly, the diaphragm is able to move down when the current
flows into the page, which is designated by the dotted vectors in the diagram.

When the diaphragm moves up, it pushes the air in front of it and creates
sound. The diaphragm can also move down, and that causes problems for us. The
down movement is what causes distortion or unwanted noise in headphones. To
combat this problem, we need mechanisms to dampen the distortion noise. This is
where the dome vent comes into play. The dome vent is a hole in the center of
the circular magnet, where air can escape through. There are also gaps and
filter paper underneath the magnet to dampen the noise. We cannot completely
eliminate the unwanted distortion, so usually a lot of engineering effort is
put into just dampening this noise.

Thus far we have discussed how a dynamic driver works, but there is another
type called a [planar magnetic driver][3]. This is not a new technology, but it
is usually found in high-end audio equipments. They differ from dynamic drivers
in the placement of the magnets and the movement of the diaphragm. As the name
suggests, magnets are aligned in two parallel planes, with the diaphragm
sandwiched in the middle. Instead of coiling the audio wire, it is usually
attached to the diaphragm in a raster pattern. There seems to be many
advantages to the planar magnetic driver, ranging from lower distortion to
better responsiveness, but they tend to be heavier due to a more hefty magnetic
construction and usually comes with a much higher price tag.

## The Experience

So far my experience with the M50X has been phenomenal. My earphones still work
pretty well, and I occasionally use them when I am on-the-go. But after trying
out the M50X for a while, I started to hear and feel the difference.

With the earphones, noise-isolating is great. It goes right into your ear
canals and you barely hear a thing around you. This means when I listen to
music, I don't need to crank up the volume. I can hear everything pretty well
through the earphones. However, I noticed the biggest annoyance with the
earphones is the unnatural reception of sounds. With a narrow ear canal, the
sounds seem to be very compacted and constrained. It was hard to clearly
distinguish one set of sounds from another. When I try to listen to the beats,
the lyrics keep interfering.

What I notice with the M50X is that sound reception is much more natural. The
ear cups are pretty big, and theres much more room for the sounds to travel in.
I could separate out the different parts of the music much easier than with the
earphones. When there are slow starts, silences, or stops in the music, it felt
much more effective and engaging. Of course, the headphones doesn't sound as
loud as the earphones at the same volume. But noise-isolation is definitely as
good, if not better, than in-ear earphones. The M50X cups are a good size, and
they fit pretty tight right over the ears.

In terms of wearable comfort, both the headphones and earphones have their
quirks. For the headphones, it fits over your ears, so you don't have a piece
of equipment stuck inside your ear canals. The headphone band fits nicely on
top of your head. And particularly with the M50X, once you really settle in and
immerse yourself in the music, the headphones seem to disappear. The downside
with the headphones is that they are definitely much heavier than the
earphones, and you will feel a bit of fatigue after a while. For the earphones,
you will have to stuck them inside your ears, so your ears might not feel as
airy. The great thing about earphones is that they are very light compared to
headphones. I can go a long time with my earphones without feeling fatigued.

Both the earphones and headphones are good audio equipments in their own right.
The earphones are light and works pretty well for what it is intended. The
headphones sound much more natural and have better handling of producing sound.
It is much more heavy and can leak a bit more sound than earphones. But the
experience is well worth it, plus the M50X comes with a neat set of cords, so I
know I'm definitely not going back.

[1]: http://www.audio-technica.com/cms/headphones/99aff89488ddd6b1/
[2]: http://www.innerfidelity.com/content/how-headphone-dynamic-drivers-work
[3]: http://www.innerfidelity.com/content/how-planar-magnetic-headphone-drivers-work
