---
title: Digging Into the Raspberry Pi
tags: [raspberry-pi, wifi]
data:
  banner: /assets/img/blog/raspberrypi_board.jpg
---

I have in my possession the old Raspberry Pi 1 Model B. I purchased this quite
some time ago, but haven't done much with it. Recently, I got involved in a
school project that will require using the Raspberry Pi as one of the
components. I thought this will be a good opportunity to pick up my Pi again
and start playing with it. Let's dust off that Pi and start digging in!

I decided to wipe my SD card and start fresh, so this post will detail how I
bootstrapped my Pi. Before we begin, we need to make sure we have a couple of
accessories. Aside from the Pi itself and the SD card, we will need a power
adapter, a wifi dongle, and a USB-to-serial cable. Any micro USB will do for
the power adapter. There is no wifi adapter built into the Model B, so we
will need to grab a wifi dongle. You might want to check out the [official
one][1]. The best and probably easiest way to interface with the Pi is using
a serial connection. So grab yourself a USB-to-serial cable, like [this
one][2]. For the rest of this post, I will assume that you are running a host
Linux system, otherwise you're on your own.

## Install the OS

First thing we need to do is install the OS onto the SD card. We are going to
use the recommended Raspbian OS since it has official support, but feel free to
use something else (like the Arch Linux port!). Head over to Raspberry Pi's
[website][3] and download a copy. In my case, I got the Raspbian Jesse image,
version May 2016. Once the download is complete, unzip it and you should get a
`.img` file. Next, plug in your SD card into your host computer and grab the
name of your card. The `gnome-disks` utility is pretty good for that, or just
mount one of your SD card partitions and use `df -h`.  The name should be
something like `/dev/mmcblk0`, and note that it is the name without the
partition part. Make sure you unmount the device. Now, we will use the `dd`
tool to write the OS image onto the SD card.

```bash
$ sudo dd bs=4M status=progress if=/path/to/raspbian.img of=/dev/mmcblk0
$ sync
```

You will need `sudo` permissions since we are writing to a device. The `if`
option should be the path to your `.img` file, and the `of` option should be
your SD card device name. The `sync` command is important to ensure that writes
actually get completely written to the device. Once this is done, we are ready
to plug in the SD card and start tweaking!

## Connecting to the Pi

Before we power on the Pi, let's wire up the USB-to-serial cable. The cable
should have four colored wires, where the red is for power, black is for
ground, white is for transmit, and green is for receive. Now take a look at
this [diagram][4] to get a sense of what the GPIO pins are on the Model B.
Let's plug the black wire into pin 6, the white into pin 8, and the green into
pin 10. With this in place, let's plug in the micro USB power adapter to power
on the Pi.

We can definitely power the Pi with just the USB-to-serial cable. However, I
feel like it will come in handy to have the power and serial separately, and
later on we can unplug one and keep the other if we need to.

It might take a few seconds for the Pi to boot up, so let's start up a serial
connection on our host computer in the meantime. First, let's list what devices
you have on your computer so far.

```bash
$ ls /dev/tty*
```

Now, plug the USB end of the USB-to-serial cable into your host computer and
issue the previous command again. You should see a new device, probably
something like `/dev/ttyUSB0`. This is the name of the serial port we will
connect to. We are going to use the `screen` tool as the serial client. It
should have probably came pre-installed on your Linux distro, but if not then just
grab it from your package repositories. In your terminal, issue this command to
start the serial connection:

```bash
$ screen /dev/ttyUSB0 115200
```

Of course, you should use the actual device name you found earlier. The
`115200` is the baud rate for the connection. The first time you fire up the
serial connection, you probably won't see anything, so just press the Enter
key. If the Pi finished booting up, you should see a login prompt. The default
user is `pi` and default password is `raspberry`.

## Some Configurations

Once you are logged in, we can do a bit of configuration using the provided
`raspi-config` tool. This is optional and can be skipped. However, it should be
quick, so fire up the tool with this:

```bash
pi@raspberrypi$ sudo raspi-config
```

I will recommend running `1 Expand Filesystem` to get the most out of your SD
card. Then, use `3 Boot Options` to change startup into `B1 Console`. Lastly,
use `5 Internationalisation Options` to change your locale (just so you know,
the default is British). Feel free to change other options as well, then
restart your Pi:

```bash
pi@raspberrypi$ sudo reboot
```

## Connect to Wifi

Once you have rebooted and logged back in, we move on to the most essential
step: connecting the Pi to the Internet. First, let's plug the wifi dongle into
one of the USB ports. If you have a Model B like me, it will likely cause the
Pi to reboot due to the dongle's power draw. Once you're ready to go, we will
start by editing the network interfaces config file:

```bash
pi@raspberrypi$ sudo vi /etc/network/interfaces
```

Near the top of the file, change the `auto` line, or add it if it's missing:

```
auto wlan0
```

And make sure you have these lines near the bottom of the file:

```
allow-hotplug wlan0
iface wlan0 inet dhcp
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
```

This will tell the Pi to bring up the wifi on boot, and to use DHCP. Save and
close the file. Next, we will edit the `wpa_supplicant` config file specified
in the above lines:

```bash
pi@raspberrypi$ sudo vi /etc/wpa_supplicant/wpa_supplicant.conf
```

We will need to specify the parameters for the network we want to connect to. I
will list the parameters I used to connect to my school's wifi, which uses
802.1X/WPA2. There are plenty of guides out there for different types of
networks. You can also use the command `sudo iwlist wlan0 scan` to see what
networks your wifi dongle is picking up.

```
network={
    proto=RSN
    key_mgmt=WPA-EAP
    pairwise=CCMP
    eap=PEAP
    phase2="auth=MSCHAPV2"
    ssid="NETWORK_NAME"
    identity="your.id"
    password="your_awesome_pw"
}
```

Add your network parameters in a `network` block at the bottom of the file,
then save and close the file. Now we can restart the wifi interface and it
should automatically connect to your network:

```bash
pi@raspberrypi$ sudo ifdown wlan0
pi@raspberrypi$ sudo ifup wlan0
```

You should see some DHCP messages flying around and the IP address that the Pi
got from your network. Our Pi is now connected to the Internet! Try out these
commands to make sure wifi works:

```bash
# check out the wifi settings
pi@raspberrypi$ iwconfig

# test out the wifi connection
pi@raspberrypi$ ping google.com

# fetch some updates and start installing packages
pi@raspberrypi$ sudo apt-get update
```

## Other Tips

You are now all set to go with your Pi. However, there might be two other
things you might find useful when working with the Pi.

### Root User

There is a root user on the Raspberry Pi, just like any other good Linux
installs. The `pi` user has `sudo` privileges, so you probably won't need to
use the root account. However, you should setup the password for root, just to
be secure:

```bash
pi@raspberrypi$ sudo passwd root
```

It will prompt you for a new password, so just type in something awesome.

### Resize Serial Console

We have been using `screen` to establish a serial connection to the Pi. You are
probably frustrated by now at the tiny console real estate you have using
`screen`. In fact, this is actually a problem with the serial connection
itself, since serial can't really tell how big your client terminal is. To fix
this, we will use the `resize` tool, which is part of `xterm`. First, let's
install the `xterm` package:

```bash
pi@raspberrypi$ sudo apt-get install xterm
```

Now, issue the `resize` command and you should have the full real estate of
your client terminal!

[1]: https://www.raspberrypi.org/products/usb-wifi-dongle/
[2]: http://www.amazon.com/JBtek%C2%AE-WINDOWS-Supported-Raspberry-Programming/dp/B00QT7LQ88
[3]: https://www.raspberrypi.org/downloads/raspbian/
[4]: http://www.raspberry-projects.com/pi/pi-hardware/raspberry-pi-model-b/model-b-io-pins
