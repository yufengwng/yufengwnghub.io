---
title: Using Iterators in Rust
tags: [iterators, rust]
---

Iterators are a powerful pattern that is very common in Rust. When you work
with standard collections such as vectors or hash maps, you will inevitably be
using iterators to traverse the collection. At it's core, iterators provide the
necessary abstraction to loop through each element of a collection. However,
iterators in Rust also enables a lot of useful functionalities, such as
functional programming style operations on collections. For example, we can use
an iterator to map over a vector to transform it into a new collection, or
filter the elements in the collection based on some criteria.

## What is an Iterator?

Simply put, an iterator in Rust is a data structure that implements the
`Iterator` trait. Typically, the structure holds data about the iteration state
of a collection, such as the current index into a list or the next element in
the collection. There is only one method that it needs to implement for the
`Iterator` trait, called `next()`. Here's what it looks like:

```rust
fn next(&mut self) -> Option<Self::Item>
```

The `next()` method will be called on the iterator to get the next element in a
collection. How do we know when to stop iterating or if the iterator has been
exhausted? Well, the function signature indicates that the return value is
actually an `Option`. So when we get the `None` variant, we know there are no
more elements and we should stop iterating. The `Self::Item` is an associated
type of the `Iterator` trait. What that means is outside the scope of this
post, but essentially it indicates the type of the element, or item, in the
collection we're working with.

Iterators are very often used with `for` loops. Let's take a look at an example
below.

```rust
for i in 0..10 {
    println!("{}", i);
}
```

Where's the iterator, and where is the call to `next()`, you might ask. Let's
take a closer look at what the actual `for` loop syntax is in Rust:

```rust
for pattern in iterator {
    ...
}
```

Believe it or not, the first part of a `for` loop is actually a pattern, and
the second part is an iterator. Rust will take the `for` loop and transform it
into another construct that will continuously call `next()` on the provided
iterator and match on the returned `Option` until the `None` variant. And yes,
a range in Rust is itself an iterator. The description just given is not
entirely correct, but it gives the general notion. Take a look at the
[documentation][iter-doc] to see the actual `for` loop syntax and
transformation.

Due to Rust's ownership and borrowing system, there are three forms of a value
we can have: a shared reference to a value, a mutable reference to a value, or
an owned value. Similarly, there are three forms of an iterator we can use, as
described in the following sections.

[iter-doc]: https://doc.rust-lang.org/std/iter/index.html#for-loops-and-intoiterator

## Shared Reference Iterator

The first form is an iterator that returns shared references to elements in the
collection. What this means is that the `next()` method will return an `Option`
where the `Some` variant holds a shared reference. Here's an example using a
vector collection:

```rust
let values = vec![20, 21, 22];
let mut sum = 0;

for num in values.iter() {
    sum += *num;
}

assert_eq!(sum, 63);
```

Here we have a vector of `i32`s called `values` and we're calculating the sum.
By calling the `iter()` method on a vector, we get back an iterator data
structure that implements the `Iterator` trait. Since it's an iterator that
provides shared references, `num` is a `&i32` and so we need to dereference it
before adding to `sum`. There's another way to do this summation. Remember how
we said the first part of a `for` loop is a pattern?

```rust
let values = vec![20, 21, 22];
let mut sum = 0;

for &num in values.iter() {
    sum += num;
}

assert_eq!(sum, 63);
```

Instead of using a variable name, we can use the pattern syntax to match the
reference directly and give us the `i32` as `num`.

## Mutable Reference Iterator

The second form is an iterator that returns mutable references. These are very
similar to the shared reference iterators, except we can mutate the elements
using the reference. Here's an example where we increment each element in a
vector by one:

```rust
let mut values = vec![20, 21, 22];

for num in values.iter_mut() {
    *num += 1;
}

assert_eq!(values, [21, 22, 23]);
```

By calling the `iter_mut()` method on a vector, we get back a mutable reference
iterator. And since `num` is a `&mut i32`, we can dereference and update the
number it points to.

## Owned Value Iterator

The third and last form is an iterator that returns ownership of the elements
in a collection. What this means is that the iterator actually consumes the
collection we're traversing and returns owned values on each call to `next()`.
Let's try it out to sum a vector:

```rust
let values = vec![20, 21, 22];
let mut sum = 0;

for num in values.into_iter() {
    sum += num;
}

assert_eq!(sum, 63);
```

When we invoke the `into_iter()` method, it will consume the vector and return
an iterator that has ownership of the vector. If we try to use the `values`
vector after the summation, Rust will not like it since it has already been
moved to the owned value iterator. And in this case, we do not have to
dereference `num` since `num` is an owned value of type `i32`.

## Infinite Iterator

Now that we understand what an iterator is and the different forms of an
iterator, let's take a look at what we can actually do with an iterator. Recall
we mentioned earlier that iterators enable various functional operations on
collections. A canonical functional operation is folding a collection into a
single value, which we can do using the `fold()` method on an iterator. Here is
an example where we transform a vector by doubling its values and summing them
by folding:

```rust
let values = vec![20, 21, 22];
let result = values.iter()
                   .map(|num| num * 2)
                   .fold(0, |acc, num| acc + num);
assert_eq!(result, 126);
```

In fact, summing the values of a collection is so common that there is a
`sum()` method on iterators that does it for us instead of having to do
`fold()` ourselves. We just need to annotate the return value to help Rust's
type inference.

```rust
let values = vec![20, 21, 22];
let result: i32 = values.iter().map(|num| num * 2).sum();
assert_eq!(result, 126);
```

But what's really interesting is that we can create an infinite iterator in
Rust. Using the `repeat()` function from the `std::iter` module, we can create
an iterator that will repeatably return the same value forever.

```rust
let mut forever = std::iter::repeat(20);

assert_eq!(forever.next(), Some(20));
assert_eq!(forever.next(), Some(20));
assert_eq!(forever.next(), Some(20));
```

Here, `forever` is an iterator that will always return the value `20`. Of
course, an infinite iterator is not typical very useful on its own. However,
iterators come with a long list of methods we can compose to perform powerful
operations on collections. Take a look at the [`std::iter`][iter-mod] module
documentation to learn about all the iterator traits, functions, and
functionalities.

[iter-mod]: https://doc.rust-lang.org/std/iter/index.html
