---
title: Travel to Philly
tags: [travel]
---

I went on a trip to Philadelphia, Pennsylvania last month during the week
of October 16th to October 19th. It was a short "vacation" over the weekend,
but I had a lot of fun. I just wanted to briefly share my travel to
Philadelphia last month in this post.

This was the second time I went to Philly for a national conference. Both
times I was participating in the conference for the Society of Asian Scientists
and Engineers (SASE). SASE is a national organization that has student chapters
across the nation, and focuses on professionalism, diversity, and community
service. I was with the Northeastern SASE chapter during the trip.

The travel to Philly was indeed tiring. The bus ride was well over six hours.
During the bus ride, I actually started working on my blog. I was researching
and trying out different blogging solutions, like using Ruby on Rails with
the postmarkdown gem. Obviously nothing final came out of the experiments on
the bus.

The hospitality lodging we had was quite unique as well. Since we had
a lot of people going on the trip (around twenty-three people) and our
chapter was low on budgets, we had to make accommodations. I was prepared to
spend the nights on the armchair and let the other members take the bed. As
it turned out, some of my roommates were working on homework and studying
late into the night, so I ended up taking the bed throughout the trip, which
was really nice.

The key features of the SASE National Conference were workshops and keynote
speakers, both of which were enlightening. Of course not all workshops were
relevant or helpful. We got to choose four workshops to attend, and I was
able to identify some things from my workshops that were relevant and useful
to me. The highlight for me was probably the keynote speakers. For both the
conferences that I have attended thus far, the keynotes were inspirational
and memorable. We had Miss America 2014, Nina Davuluri, as one of the keynote
speakers! She was a very well-spoken individual.

The Career Fair was perhaps the biggest event during the conference,
as it was one of the biggest fairs for Asian American and Pacific
Islanders across the nation. However, it was not really something I was
excited about. Most of the companies represented at the Career Fair were
heavily focused on engineering and science fields like mechanical, electrical,
biological, and chemical. I did get a chance to speak with some people from
NASA and GE. And probably the highlight of the fair for me: I got to put on
an Oculus Rift headset that NAVSUP brought in!

![Career Fair 2014](/assets/img/blog/philly_sase_career_fair_2014.jpg
"SASE Career Fair 2014")

Unlike last year, our members and I were actually able to take some time to
explore Philadelphia. First thing we did was order Philly cheesesteaks
for lunch, which was probably a mandatory tradition when you are in Philly. The
second thing we did was to visit the Independence Hall to see the Liberty Bell.
Last year we were unable to approach this famous location because of the
government shutdown. Unfortunately, we arrived a bit late and was only able to
stare up at the building in awe.

On our way back to our hotels, we stopped by the Franklin Square park. We saw
recommendations of it online, and it was indeed a beautiful sight. There was a
gorgeous and refreshing fountain in the middle of the park. There was also
mini golf, a merry-go-around, and a playground. We ended the night with dinner
at a nice noodle place that a local SASE member recommended. The Nan Zhou Hand
Drawn Noodle House had some pretty enticing dishes. It was actually the first
time I had shaved noodles!

![Franklin Square fountain](/assets/img/blog/philly_franklin_square_fountain.jpg
"Franklin Square Park Fountain")

Overall, it was a short and refreshing vacation for me. I was on co-op
during this time, so it was a nice break from the work schedule to take some
fresh air. And it was pleasant to learn some new things, meet some new people,
and explore some new places!
