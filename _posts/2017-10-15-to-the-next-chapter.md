---
title: To the Next Chapter
tags: [1000-days]
data:
  banner: /assets/img/blog/reflection_bay_bridge.jpg
---

Almost three years ago, I wrote a [blog post][prev-post] where I hinted about
writing a reflection post of my experiences for the past 1,000 days. Although
the yearly write-ups that I mentioned did not happen, I definitely wanted to
use this reflection to share some of my thoughts. I won't dive into details, as
I don't remember everything anyways, so I will try to focus more on themes and
the big ideas.

The rest of this post will be composed of various sections, where each will
discuss a coherent theme or item. They are not necessarily in chronological
order, but it will be close enough.

[prev-post]: /posts/to-the-next-1000-days

## College Days

A couple days ago, I was surprised to receive my yearbook in the mail earlier
than expected. As I was flipping through the pages, memories of my college days
started flowing back to me. Although I wanted to keep the topics in this post
more technically-oriented, I will like to reserve some space here to reflect on
my college experience as well.

To put it simply, a lot of things has happened during my college years. To
start us off, there were numerous events that had made an impact globally and
right here in Boston. Some of the most memorable were: the 2013 Boston
Marathon, the 2014 World Cup, both the winter and summer Olympics, SpaceX,
virus outbreaks, the 2016 Presidential Elections, various rights movements and
protests, and endless amounts of snow. Many events were on the negative side,
as these tend to be the type of topics that generally garner the most media
attention. However, there were also many occasions where people gather to
converse, celebrate or support each other, whether that be engaging in a global
sporting event or giving support to victims of a natural disaster.

On another note, some of my most memorable personal experiences stems from my
time with a student organization called SASE early on in college. It gave me an
opportunity to be with an organization that was just starting up, so I had
plenty of chances to engage with new people, practice leadership, organize
events, and contribute to the culture. I also had a number of chances to
volunteer in community service, which is something I had enjoyed doing since
high school. What's more, I even got to go on not one but two [trips to
Philadelphia][philly-post] with the rest of the members.

Another important part of college was the co-op experience. I was able to go on
three distinct internships each lasting six months. It was a substantial amount
of time that allowed me to integrate nicely into the respective teams, ramp up
on the tech stack, and complete a significant amount of work. My first co-op
was my first encounter with software engineering in the real world, and where I
picked up many of the best practices used in the industry. My second co-op
showed me what life is like at a big tech company, and what living in the West
Coast feels like. And lastly, my third co-op brought me a step closer to an
area I am interested in pursuing further. In short, each of the three co-ops
had something to offer.

Next, I will like to touch upon a few technical topics that was of interest to
me during these past 1,000 days.

[philly-post]: /posts/travel-to-philly

## From Ruby to Rust

Early in college, I had a little programming languages "fever" of some sort
mainly due to Ruby. At that time, I had heard of the Ruby programming language
here and there, but it wasn't until I came across [_why's poignant
guide][wpgtr] that I started to realize how much fun Ruby can be. The syntax and
idioms was a bit odd at first, but I gradually came to appreciate the beauty of
the language and its focus on programmers. In a way, learning Ruby made me
rethought about what's possible with programming and developer tooling. As much
as I liked it, I didn't get much chances to do something significant with Ruby.

Shortly after, the [Rust programming language][rust-lang] came along. I don't
recall how I exactly discovered it; it just sort of happened. And this was
before the 1.0 release of the language. The more I learned about Rust, the more
excited I became. It was as if they had took all the good ideas from other
languages and stuffed them all into Rust. And like Ruby, learning Rust changed
my view of what's possible with systems programming. Rust does have a
particularly steep learning curve, so it took some time before I started
writing some actual code. I had started to use it more frequently, and hope to
continue to do more with it.

[wpgtr]: https://poignant.guide/
[rust-lang]: https://www.rust-lang.org/

## Learning from Learning

During the latter half of college, I enrolled in an introductory course on
machine learning. It was one of the tougher courses I took, since it was hard
at first to wrap my head around the concepts. But once it starts to make some
sense, I was opened up to a world of fascinating ideas. How in the world can
you simply apply an "algorithm" that basically equates to a couple math
formulas, crunch some numbers, and get software that says whether someone will
get admitted to grad school or not. Of course things are never that simply or
straightforward, but with machine learning we can ask evermore complex and
ambiguous questions. In a way similar to Ruby and Rust, learning about machine
learning changed my view of what's possible with software.

I had some downtime after graduating from college, so I took the opportunity
to augment my understanding of machine learning. Like so many others, I
invested my time going through Andrew Ng's excellent [Coursera course][ng-ml]
on the subject. The progression of the course was different from the one I took
in college, and gave me the benefit of learning the same subject differently.
Learning the concepts and algorithms a second time around definitely helped in
clarifying some ambiguities and correcting some assumptions.

One key idea I got out of machine learning so far is the perspective that the
world is probabilistic. People studying physics know this already, but for the
rest of us it's a good idea to think of the world in probabilistic terms. The
world is never black and white, or good and bad; there is always something
in-between. You can plan all you want, but there will still be a bit of
uncertainty; after all, no one really knows the future. And so probability
theory is a good way to model the world we live in. At first this seems to
clash with a typical programmer's world-view, since we tend to like things to
be deterministic. Of course we should have well-defined behavior, but we also
need to account for change and uncertainty.

A well-known algorithm in the field was inspired by the learning process in the
biological brain. Although this algorithm is a naive model of how the brain
actually works, it tends to work surprisingly well for a variety of problems.
Discovering this, it got me curious about the brain, cognition, and how we
humans actually learn. I even participated in a project about brain-computer
interfaces for my senior capstone project in college.

[ng-ml]: https://www.coursera.org/learn/machine-learning

## Compilers are Everywhere

One little regret I have was missing out on the compiler course in college. I
was working on a dual degree, so my schedule wasn't able to fit the course
and it's prerequisites. But since I had some downtown after graduation, I
started to pick up on the subject. It seemed daunting at first, but after
reading about it and having it explained on paper made me realize that a
compiler is just another program. It employs some distinct algorithms, data
structures, and optimization techniques, but in the end it's really not that
much different.

After learning about various programming languages for a while, it is natural
that I started to be curious about how these languages are implemented and what
goes under the hood. I'm sure that other programmers also had the urge at some
point in their lives to write a compiler or interpreter for their own
programming language. In fact, it seems to me that there is a wave of new
programming languages in recent years. We got new compiled programming
languages like Rust, Swift, and Go. There's new JVM-based languages like Scala
and Kotlin. New web languages like Dart and TypeScript. And many more niche and
"little" languages. Either there is an actual trend or it's just me being more
sensitive about this topic.

Having started learning about compilers, I realized that they are extremely
common and underpins most tools we use. Well, it's more correct to say
_parsers_ are very common. A parser is a component of a compiler or interpreter
that transforms plain text (aka source code strings) into data structures that
the compiler can work with. Broadly speaking, strings is a universal interface
between users and most programs we write. The user provides input via text
strings, and the program uses a parser to process it in order to figure out
what the user had specified. Parsers can be as sophisticated as the ones in
compilers, or as simple as a string-to-integer converter. We see parsers used
in compilers, terminal shell programs, command-line applications, data
serialization, data languages like GraphQL, and many others. Even if we change
the paradigm of how we write programs, I think parsers will continue to play a
vital role.

At the moment, I'm working through Bob Nystrom's fantastic [handbook][ci-book],
and I highly recommend it. The section about tree-walk interpreters was a nice
start, and I'm looking forward to the section about bytecode virtual machines.

[ci-book]: http://www.craftinginterpreters.com/

## Wrapup

I must admit that I did not spend enough time thinking, writing, and editing
this post as I should have, so it must have been a rough read. Thanks for
sticking with me and making it this far. Stay with me a little longer, and I
will reward you with a little quote.

Now, you might be expecting me to reiterate some points I made above to hammer
it home. Well, there is none of that. Because now it is no longer about the
past 1,000 days but it's about the next 1,000 days. In fact, I'm quite excited
about what's coming up, since just around the corner I will be starting my
first full-time job out of college. I haven't decided if I will do another one
of these reflection posts; maybe I will forget about it, or maybe I will round
it up a couple months instead of just 1,000 days.

Well, that's all from me. Here, let me end with a nice quote. Perhaps I will
establish a tradition of always ending this type of posts with a quote.

> Define success on your own terms, achieve it by your own rules, and build a
> life you’re proud to live.
>
> *- Anne Sweeney*

