---
title: Finishing What You Started
tags: [hiking]
data:
  banner: /assets/img/blog/hike_el_cajun.jpg
---

When you underestimate something, you will need to pay the price for it. That
does not mean you cannot overcome the obstacle in front of you, though. And
when it comes to hiking, all you need is a little push from the people around
you to finish what you began.

It was early Sunday morning, the weather was expected to be a little on the hot
side. I got out of bed early that day to join my party of three for a hike. We
headed east towards [El Cajun Mountain][1]. We got lost on our first try.
Google Maps took us to some mountain dweller's backyard. There was an alarming
private property sign, so obviously something was not right. We backed out,
searched for the place again, and pulled into a parking lot on our second try.
There was two other parties in the parking lot. Everyone there was gathering in
their group and tightening up their backpacks. Looks like we're in the right
place.

The night before, my roommate invited me to join the hike. It was a trip that
just got brought up that day. I thought it wouldn't be so bad, since this will
be my second time hiking in San Diego. For our first hike back in August, we
went to the [Los Peñasquitos Canyon Preserve][2]. It was a relatively flat
trail, so it was pleasant for beginners like me. Plus, the scenery was lush
green, with calm flowing creeks, and a nice little waterfall at the end. But
the El Cajun trail was different. I didn't have a clue. My roommate did warn me
that it was an eleven mile hike, but I didn't read much into it. I was
negligent and did not Google the trail to prepare myself for what's coming.

![Water creek of Los Peñasquitos](/assets/img/blog/hike_los_penasquitos.jpg
"A Water Creek on the Los Peñasquitos Hike")

That morning, I slapped on a set of casual clothes, packed my bag with water
and granola bars, then headed out with the group. When we pulled into the
parking lot, I looked around and noticed everyone was either wearing polyester
or nylon shorts. I looked down and see a pair of jeans. Bad idea. I should have
put more thought into it. So I rolled up the end of my pants just to make
myself feel a little better. We started heading out, and the first thing we
encountered was a long, paved, forty-five degree inclined ramp. We saw a
Mustang gunned up the ramp, so it definitely does not seem like a pleasant
climb. There was a little resting area with bathrooms atop the ramp. I was
already a little tired after that ramp. I feel my blood running faster and my
breathing becoming heavier.

The trail to the summit consisted of three main slopes of increasing length,
with several ups and downs to start it off. We took a small break at the
resting area, then started up the trail. Before we knew it, we were already
past the mile one mark and the first set of ups-and-downs. We reached a small
little plateau and decided to take a rest. I was already dead tired, and my
legs were pretty much used up.

Something doesn't seem right, and I don't have a good feeling about it, so I
addressed my party. I told my group that they should go on without me, and
suggested that I will stay behind to rest and possibly head back to the car.
They refused to leave me behind, so I really appreciated that. However, I had a
strong feeling that if I were to continue, I might not be able to make it back
home. Suddenly, my head hurled to the left, and I vomited onto the little bush
below. I was not expecting that, and neither was my group. I was glad that it
was just water, but nonetheless it was a sign that I have to stop. My group
headed on without me, and we kept in touch on the phone.

I cleaned myself up, and took my time to absorb the scenery around me. After
strolling around and taking a few pictures, I started feeling the energy coming
back to my legs. I don't recall how long I rested, but I was feeling much
better. I actually felt better than when we started. With this renewal, I was
thinking I could actually make it back to the car now. I took another look
around at the scenery. Then I asked myself, "When will I get another chance to
conquer this trail?" I'm not staying in San Diego for too long, but I can
always come back maybe in a year or two. However, since I'm here already I
might as well finish what I started. I decided to get back on track to reach
the summit, even if I have to go at a really sluggish pace.

After analyzing my journey thus far, I realized that the group pacing was a bit
too fast for me, and I was not taking enough breaks in-between. I messaged my
group that I will be continuing, but at my own pace. They informed me that they
are just ahead of me, taking a rest. I started to head out, taking occasional
breaks during climbs. I wasn't expecting to catch up to my group at any time,
but turns out my group was actually waiting for me atop the first major slope.
Once I met up with them there, they offered to slow down the pace and insisted
that we progress as a team. Agreeably, it will be more rewarding to reach the
summit as a team. I was very grateful for their consideration, so I tried to
keep in range of the group pace. Throughout the rest of the trail, I kept in
view and in hearing distance, albeit being the last one among the group.

![Sign showing the mile 5 mark](/assets/img/blog/hike_mile_five.jpg
"The Mile 5 Mark and Elevation Profile")

We finally made it to the mile five mark, a significant point leading to the
top of the hike. The rest of the way towards the summit was a rather rocky
path. The trail got much thinner, and there were bigger jumps of elevation in
the trail. As we close in on the summit, everyone in the group was starting to
feel a little eager and began picking up the pace as the summit came into view.
I made my step onto the summit, straightened my back, and feasted my eyes upon
the vast open blue sky.  The mountain ranges look like little bumps, and the
buildings look like Monopoly pieces. The view was magnificent and refreshing.
We sat atop the tallest boulders we found and started to enjoy our lunches. I
took a couple pictures, and an obligatory Photo Sphere. We stayed at the summit
for quite a while, taking our time to appreciate the scenery and trying to spot
Mexico.

The later half of the hike was perhaps the most difficult part. It took us
three tries to come down from the summit back to the mile five mark. The path
was very rocky and seems to disappear sometimes, so it was hard for us to keep
on track. On the bright side, we were greeted by a husky on our way down. One
of the other hikers coming up the summit also brought his husky along. It was a
pleasant encounter, since huskies are a pretty rare sight, despite coming from
a university with a husky mascot.

![Sighted a husky during the hike](/assets/img/blog/hike_husky.jpg
"Unexpected Appearance of a Husky")

Once we came back to the mile five mark, my group suggested to head up the
other road of the fork towards another trail. I decided to stay behind at the
fork, in order to conserve energy for the descent. After my group came back,
they tried to convince me to take a detour. I thought it wouldn't be so bad if
it's only a mile out. I was also curious since they mentioned we will be able
to observe El Capitan's cliff face. We went towards this detour and ended up
going more than a mile out. We got to the end of some cliff, but there was no
El Capitan cliff face in sight. Someone must have gave us false information.
The only reward we got was a view of the El Capitan Reservoir. After a little
break, we started to head back.

I'm not sure how I convinced myself that I will be able to survive the descent
from the summit. All I knew was that I can't just stop there. Adding the
detour, we will have averaged about fifteen miles for this hike, so inevitably
my legs switched to auto-pilot mode. Occasionally, I will stop abruptly to take
a break. Then my legs will suddenly start moving me along. Throughout the
entire way down, the only thing on my mind was getting back to the parking lot.
The trail seemed endless. The sun was starting to retire, and the blue sky
slowly fades away. Something is starting to come into sight. I see a picnic
table, some benches, and a small shack with bathroom labels. We're finally back
at the resting area. I walked right past the resting area, swung around to the
other side, and stopped. Oh right, there is still this insanely steep ramp. The
final stretch before meeting my group in the parking lot. Goodbye, ramp.

![The steepest ramp I have encountered](/assets/img/blog/hike_ramp.jpg
"Looking Back at the Steep Ramp")

[1]: http://www.modernhiker.com/2015/01/06/hike-el-cajon-mountain/
[2]: http://www.modernhiker.com/2014/06/24/hiking-los-penasquitos-canyon/
