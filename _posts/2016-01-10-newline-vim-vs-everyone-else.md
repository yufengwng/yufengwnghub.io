---
title: "Newline: Vim vs Everyone Else"
tags: [atom-editor, unix, vim]
---

In the text editor world, there seems to be two camps of editors in regard to
how they handle newlines. In one camp, you have vim. In the other camp, you
will find most of the modern text editors, like Atom and Sublime Text, and even
editors in IDEs like Eclipse and IntelliJ.

I'm going to pick on Atom, since I got it installed recently. Atom is an
awesome editor. It looks pretty, has a nice package manager, and provides
various configuration options. I'm a frequent vim user, but I decided to get
Atom as a backup editor for whenever I need a GUI. Plus, Atom comes with a
Markdown preview feature, which will be nice when I'm editing Markdown. When I
opened Atom to edit a file, the first thing I noticed was this extra line at
the end of the file. I opened the same file in vim, and it looks just like how
I have written it. Atom is definitely doing something different and it's a bit
annoying.

I did some Google-ing and searched through GitHub about this odd newline
behavior. It turns out other people noticed this newline behavior as well and
there's quite a few discussions about it. I found this [GitHub issue][1] and
this [StackOverflow question][2]. In particular, I learned quite a bit about
newline and Unix history from that SO question.

In order to clearly see the difference in vim and Atom, I did a little
experiment. In the experiment, I'm going to create a new file in both vim and
Atom, write some text, save the file, and then compare the two files. To
facilitate this, here is a quick Ruby script that handles setting up the
experiment. Currently, I only compare the file type and line/word/character
count.

```ruby
# newline.rb
# Test out newline behavior in vim and atom.

VIM_FILE = "newfile_vim.txt"
ATOM_FILE = "newfile_atom.txt"

def prompt(msg)
  print msg
  gets
end

def set_up
  system "touch #{VIM_FILE}"
  system "touch #{ATOM_FILE}"
  system "vim #{VIM_FILE}"
  prompt "press enter after editing vim file "
  system "atom #{ATOM_FILE}"
  prompt "press enter after editing atom file "
end

def tear_down
  system "rm -f #{VIM_FILE}"
  system "rm -f #{ATOM_FILE}"
end

def comp_file_type(files)
  return if files.empty?
  puts "\n# File Type"
  puts `file #{files.join " "}`
end

def comp_word_count(files)
  return if files.empty?
  puts "\n# Word Count"
  puts "l w c filename"
  files.each { |f| puts `wc #{f}` }
end

def test
  set_up
  comp_file_type [VIM_FILE, ATOM_FILE]
  comp_word_count [VIM_FILE, ATOM_FILE]
  tear_down
end

test
```

To run the experiment, I will run `ruby newline.rb` and edit the new files in
vim and Atom. Of course, as a big vim fan, I have the `vim-mode` and `ex-mode`
packages installed in Atom, giving me the vim commands and modal interface in
Atom. Thus, for both vim and Atom, I will essentially follow the same steps to
edit the new files:

1. Press `i` to go into insert mode.
2. Type some text.
3. Press `Esc` to go back to normal mode.
4. Type `:wq` to save and close file.

With the groundwork ready, let's run an experiment. For this trial, we are only
going to press enter for step 2 of the editing procedure. Here is the output:

```
yufeng@linuxbox:ruby/exp$ ruby newline.rb
press enter after editing vim file
press enter after editing atom file

# File Type
newfile_vim.txt:  ASCII text
newfile_atom.txt: very short file (no magic)

# Word Count
l w c filename
2 0 2 newfile_vim.txt
1 0 1 newfile_atom.txt
```

Let's run another experiment but this time we are going to write some text:
`This is a new file.`. Here is the output:

```
yufeng@linuxbox:ruby/exp$ ruby newline.rb
press enter after editing vim file
press enter after editing atom file

# File Type
newfile_vim.txt:  ASCII text
newfile_atom.txt: ASCII text, with no line terminators

# Word Count
l w c filename
 1  5 20 newfile_vim.txt
 0  5 19 newfile_atom.txt
```

From what I learned in my minimal research and experiment, I like to believe
vim does it right with newlines and has a behavior that I will prefer. In
general, I think other text editors have two "misconceptions":

1. Newlines are interpreted as starting a new line.
2. Files must end with a newline character.

From the SO question mentioned above, I learned what a "line" means in the
traditional Unix sense. A line is defined to be a "sequence of zero or more
non-newline characters plus a terminating newline character." Thus, newlines
are suppose to indicate the end of a line and not the starting of a new line.

If we take a look at the output of the first experiment, we will notice that
the vim and Atom files have different number of lines. For vim, there are two
lines, while the Atom file has only one line. If we take a look at the output
of the second experiment, we will notice that the Atom file has one less
character than the vim file. That missing character must have been the newline
character. From these results, we can deduce the behaviors in vim and in Atom.

For vim, every line ends with a newline character. Pressing enter in vim will
create a new empty line, which only has the newline character. For Atom, every
line does not end with a newline character unless you press enter. Pressing
enter in Atom will insert the newline character and bring you to a new empty
line. This new empty line will not have a newline character. In this sense, vim
follows the traditional Unix definition of a line, while Atom interprets
newline as starting a new line, indicating the first misconception that I
listed above.

For the second misconception, some text editors try to enforce having a newline
character at the end of files. Certainly Atom does it, and has the `whitespace`
package to handle it. I think this is solving the wrong problem. If we adhere
to the definition of a line, there's no need to worry about files not ending
with a newline character, since that will be guaranteed. A text file can be
defined as a sequence of lines, and since each line ends with a newline
character, the last line will always have a newline character. Thus, fixing the
first misconception will automatically fix the second misconception.

Of course, it isn't quite fair to say that the Atom text editor has
misconceptions, since I forced the Unix standards on it. I suppose Atom just
does things differently. On the other hand, vim is built with Unix standards in
mind, so its behavior seems reasonable when used with Unix tools like `file`
and `wc`. I personally prefer what vim offers. I guess I will be sticking to
vim for a lot more things, but it's not like I plan otherwise.

[1]: https://github.com/atom/atom/issues/7787
[2]: http://stackoverflow.com/questions/729692/why-should-text-files-end-with-a-newline
