(function(window, document, $) {

  $(document).ready(function() {
    // Add styling to generated blog post.
    $('.blog-post').find('img').addClass('ui image');
    $('.blog-post').find('table').addClass('ui celled table');
  });

}(window, document, jQuery));
